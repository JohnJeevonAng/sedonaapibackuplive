-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 21, 2015 at 10:06 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbbnb`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(250) NOT NULL,
  `task` varchar(20) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `birthday` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `country` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `profile_pic_name` varchar(250) DEFAULT NULL,
  `activation_code` varchar(150) DEFAULT NULL,
  `status` varchar(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `task`, `first_name`, `last_name`, `birthday`, `gender`, `country`, `state`, `profile_pic_name`, `activation_code`, `status`, `created_at`, `updated_at`) VALUES
('03686d5c-bd04-45a4-d8ff-5f7a00ee23df', 'superagent', 'efrenbautistajr@geeksnest.com', '7c222fb2927d828af22f592134e8932480637c0d', '', 'efren', 'bautista', '1986-02-15', 'male', 'Philippines', 'Here', NULL, NULL, '1', '2015-04-20 16:09:57', '2015-05-13 06:47:41'),
('14F6A274-19C8-4AC6-B03C-79A74FF7DE67', 'user', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, 'BA1E4140-C6BC-41E8-A078-BB69E34FA3A9', '0', '2015-05-21 05:32:54', '2015-05-20 21:32:54'),
('1C54E286-DE95-4C9B-8B7B-49DCE1F0C7DA', 'user12', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, 'FBFE036F-DCD0-4ABF-A6A6-6DDD9199FD74', '0', '2015-05-21 05:32:59', '2015-05-20 21:32:59'),
('4E681182-9628-4226-9714-AC873B3B3CFB', 'asdfsadf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, 'F11C1631-734A-49B0-9200-737F985D0A7F', '0', '2015-05-21 05:33:08', '2015-05-20 21:33:08'),
('520DD97F-D7C8-4663-813C-A0CF6ECED09E', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, 'A48728D2-B385-4408-A0EB-016F0D3A6D3E', '0', '2015-05-21 05:33:23', '2015-05-20 21:33:23'),
('57A43144-6EA2-4957-BA60-C97261FEE2AE', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, 'ED17D11F-F72F-4BD6-9A50-5BD897C2C786', '0', '2015-05-21 05:33:20', '2015-05-20 21:33:20'),
('5CEE91A0-810D-43A0-A570-6FBEAC52D76D', 'kyben', 'flipt_silent01@yahoo.com', 'b174961e9153e0354225c882bd47541b8c8c8e95', 'Editor', 'kyben', 'de GUia', '1992-10-01', 'Male', NULL, NULL, NULL, '07115DF4-4155-429F-B323-637BA858D59D', '1', '2015-05-21 06:29:13', '2015-05-20 22:29:13'),
('6AA12230-23D7-43E7-A839-BD5E2BC390B9', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, '20F00711-0E60-4998-88A3-423558417649', '0', '2015-05-21 05:33:21', '2015-05-20 21:33:21'),
('6CCE3021-3648-4226-B8DE-4D26626C4F30', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, '17B35747-A45F-43C4-AD44-C130025B9611', '0', '2015-05-21 05:33:21', '2015-05-20 21:33:21'),
('7421C9DC-810D-40AE-A3BA-A9177FC0E788', 'dfgadfgfd', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, 'D235D9BE-7F8E-4411-9362-BEB302D99125', '0', '2015-05-21 05:33:14', '2015-05-20 21:33:14'),
('7B715D6A-F1D3-45BF-9596-705C15F2F58F', 'kyben edit', 'editkc.deguia@gmail.com', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 'Admin', 'kyben edit', 'de Guia edit', '1992-10-01', 'Male', NULL, NULL, NULL, '26DFFD03-66EF-4116-B3E8-050584D15427', '1', '2015-05-21 08:08:36', '2015-05-21 00:08:36'),
('82BA75B9-7906-486B-870B-D0246CCB4A61', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, 'D53E33DA-A213-4278-BE64-61CDA0944E61', '0', '2015-05-21 05:33:22', '2015-05-20 21:33:22'),
('91F928FC-F9C9-47CE-8269-804907438AB4', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, '3A7836B0-38B1-44AB-BC68-901425AEF744', '0', '2015-05-21 05:33:23', '2015-05-20 21:33:23'),
('924302C1-2F70-4B97-9D0E-280A30270D29', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, 'CADAB31C-4FD1-487E-8798-14E6165007A8', '0', '2015-05-21 05:33:20', '2015-05-20 21:33:20'),
('948D4EB6-9B79-4203-B811-E19B1B19631E', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, '49B160F3-8EF8-4917-B410-9AD5C619AD8F', '0', '2015-05-21 05:33:19', '2015-05-20 21:33:19'),
('96B8198C-8F30-46BF-B205-0F05824F5107', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, 'D5BE5B9A-924F-4951-931A-81BB51587D32', '0', '2015-05-21 05:33:20', '2015-05-20 21:33:20'),
('9D8C72A6-0144-4977-B71F-C044FF5A6C33', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, 'F2229B9B-D37F-49CB-9A3F-34E2232C13EC', '0', '2015-05-21 05:33:21', '2015-05-20 21:33:21'),
('A2792E67-D05D-43D9-B1D8-94057A39D599', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, '56E3E891-7D00-4042-80FB-2561A448D53E', '0', '2015-05-21 05:33:20', '2015-05-20 21:33:20'),
('A5CFF7A0-86A8-468B-A6D3-A918441DEDCF', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, 'AC2FD605-D662-4581-8FA4-59E1AC7A266B', '0', '2015-05-21 05:33:20', '2015-05-20 21:33:20'),
('AD32BC45-BDB8-4AAA-AF2F-3D6E0A172CBB', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, 'CA3B08DA-F736-4384-90B6-1A24736ABCA2', '0', '2015-05-21 05:33:23', '2015-05-20 21:33:23'),
('B6DF195B-6C8F-4E76-AF69-5A7BA95FA6DD', 'kyben', 'kc.deguia@gmail.com', 'b174961e9153e0354225c882bd47541b8c8c8e95', 'Admin', 'kyben', 'de Guia', '1992-10-01', 'Male', NULL, NULL, NULL, '60BC3409-6558-46F8-B6B9-B99FD864525B', '1', '2015-05-20 08:12:23', '2015-05-21 05:51:50'),
('BF363A97-EB53-4FC7-9542-5439830A12AD', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, '590BE7DA-0EAD-4CCB-9CBE-79838BF0CC38', '0', '2015-05-21 05:33:19', '2015-05-20 21:33:19'),
('C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 'ebautistaj', 'efrenbautistajr@gmail.com', '13ec182672f07a51b64d88483486189000cc4d87', '', 'efren', 'bautista', '1999-03-15', 'male', 'Angola', 'Bie', NULL, '', '1', '2015-04-20 16:09:57', '2015-04-20 08:09:57'),
('C3ADEAAB-F44F-44CA-B699-776AA87AAB02', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, 'A0293C23-A184-4BF7-A891-B72F197FA3A9', '0', '2015-05-21 05:33:23', '2015-05-20 21:33:23'),
('C9B4370B-4E7D-4CC9-BEF6-4410F1FEC1AE', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, '0E1D9C75-3959-46FE-99E5-BEF3A9100245', '0', '2015-05-21 05:33:22', '2015-05-20 21:33:22'),
('CEDC4FA1-3079-4ACF-A747-303E8768A086', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, '1D0FB0B0-8BB7-4FA6-B90B-8E6AC6348212', '0', '2015-05-21 05:33:22', '2015-05-20 21:33:22'),
('CEE1CAF7-8741-403C-81AD-981F5A3ADA40', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, '13336C5A-FDB4-4943-AA02-F53F41FBBB08', '0', '2015-05-21 05:33:17', '2015-05-20 21:33:17'),
('DD53EEFA-5137-4F96-B05C-DEB5C6AA6D88', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, '7D30F64F-2AD4-44E0-A1DF-7B0ECB14E4A9', '0', '2015-05-21 05:33:22', '2015-05-20 21:33:22'),
('DE88C935-18FE-404B-85F1-F9D7948E7D8D', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, 'C232F21F-0840-4586-B510-93FCD0480EB9', '0', '2015-05-21 05:33:20', '2015-05-20 21:33:20'),
('DF9E6F12-9123-461E-92EB-D06BEA4F9737', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, '5EC4458B-9E21-4F6F-85A2-C4BB99F166C1', '0', '2015-05-21 05:33:22', '2015-05-20 21:33:22'),
('E4713622-7ACD-44C9-8BA8-0826A51E0EBF', 'byken', 'asd@asd', 'f10e2821bbbea527ea02200352313bc059445190', 'Administrator ', 'kyben', 'de guia', '1992-10-01', 'Male', NULL, NULL, NULL, 'C6DB0E4A-88EB-4E08-9C8F-8D0A9DE4FF29', '0', '2015-05-21 09:48:44', '2015-05-21 01:48:44'),
('EB48AB84-76CC-4978-A5F2-C53BBBA071FE', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, '9BE519DE-F231-48E6-A86E-4CD00BC9BBF5', '0', '2015-05-21 05:33:23', '2015-05-20 21:33:23'),
('ECD50465-790B-4CF1-B80A-1CBBFC2764CD', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, '9F25C265-0E57-4CE6-AF83-D012583F8595', '0', '2015-05-21 05:33:19', '2015-05-20 21:33:19'),
('EEF6D76B-DCC7-4707-9D8F-0EF9C96E37D2', 'dfgadfgfddsf', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, '008D7CAA-AC3B-4B4E-A957-563DACFE8E72', '0', '2015-05-21 05:33:18', '2015-05-20 21:33:18'),
('F3B34D64-78E7-4293-873E-244EBCAA2C8B', 'user12sadfsad', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, '6C02DA6B-FBEE-4F9A-830F-FA33BEE179AC', '0', '2015-05-21 05:33:03', '2015-05-20 21:33:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

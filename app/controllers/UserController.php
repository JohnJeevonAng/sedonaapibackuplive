<?php

namespace Controllers;

use \Models\Users as Users;
use \Controllers\ControllerBase as CB;
use \Models\Forgotpassword as Forgotpassword;

class UserController extends \Phalcon\Mvc\Controller {


    public function userExistAction($name, $id) {
        if(!empty($name)) {
            $condition = 'username="' . $name . '"';
            if(!empty($id)){
                $condition .= 'AND id!="' . $id . '"';
            }
            $user = Users::findFirst($condition);
            if ($user) {
                echo json_encode(array('exists' => true));
            } else {
                echo json_encode(array('exists' => false));
            }
        }
    }
    public function emailExistAction($name, $id)
    {
        if (!empty($name)) {
            $condition = 'email="' . $name . '"';
            if(!empty($id)){
                $condition .= 'AND id!="' . $id . '"';
            }
            $user = Users::findFirst($condition);
            if ($user) {
                echo json_encode(array('exists' => true));
            } else {
                echo json_encode(array('exists' => false));
            }
        }
    }
    public function validatePasswordAction($id, $password){
        $user = Users::findFirst('id="' . $id .'"');
        if($user){
          if($user->password == sha1($password)){
            echo json_encode(array('valid' => true));
          }else {
            echo json_encode(array('valid' => false));
          }
        }
    }
    public function activationAction()
    {
        if (!empty($_POST['code'])) {
            $code = $_POST['code'];
            $user = Users::findFirst('activation_code="' . $code . '"');
            if (isset($user->activation_code)) {
                $user->activation_code = '';
                $user->status = 1;
                if($user->save()){
                    echo json_encode(array('success' => 'Your account have been fully activated. You can now login.'));
                }else{
                    echo json_encode(array('error' => 'Cannot update record.'));
                }
            } else {
                echo json_encode(array('error' => 'No activation code found.'));
            }
        }
    }
    public function registerUserAction(){

        $request = new \Phalcon\Http\Request();
        //var_dump($request->getPost('username'));
        if($request->isPost()){
            $username = $request->getPost('username');
            $email = $request->getPost('email');
            $password = $request->getPost('password');
            $userrole = $request->getPost('userrole');
            $firstname = ucwords(strtolower($request->getPost('fname')));
            $lastname = ucwords(strtolower($request->getPost('lname')));
            $profpic = $request->getPost('profpic');
            $bday = $request->getPost('bday');
            if(isset($_POST['status'])){
                $status = $_POST['status'] == true ? 1 : 0;
            }else {
                $status = 0;
            }

            $bday = $request->getPost('bday');
            $gender = $request->getPost('gender');
            // $country = $request->getPost('country');
            // $state = $request->getPost('state');
            /* Register User*/
            $guid = new \Utilities\Guid\Guid();
            $usave = new Users();
            $usave->id = $guid->GUID();
            $usave->username = $username;
            $usave->email = $email;
            $usave->password = sha1($password);
            $usave->task = $userrole;
            $usave->first_name = $firstname;
            $usave->last_name = $lastname;
            $usave->birthday = $bday;
            $usave->gender = $gender;
            $usave->status = $status;
            $usave->profile_pic_name = $profpic;
            $usave->activation_code = $code = $guid->GUID();
            $usave->created_at = date("Y-m-d H:i:s");
            $usave->updated_at = date("Y-m-d H:i:s");

            if(!$usave->create()){
                $errors = array();
                foreach ($usave->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            }else{
                // $app = new CB();
                // $content = 'Welcome to Planet Impossible, '.$firstname. ' '. $lastname . ' <br>  <br> Please click the confirmation link below to complete and activate your account.
                // <br><br>  Link: <br><a href="' . $app->getConfig()->application->baseURL . '/registration/activation/'.$code.'">' . $app->getConfig()->application->baseURL . '/registration/activation/'.$code.'</a><br><br> <br><br>Thanks,<br><br>PI Staff';
                // $app->sendMail($email, 'PI Confirmation Email', $content);
                echo json_encode(array('msg' => 'New User has been successfuly created.', 'type'=>'success'));
            }
        }else{
            echo json_encode(array('msg' => 'No post data.', 'type'=>'error'));
        }
    }

    public function loginAction($username,$password) {

        $request = new \Phalcon\Http\Request();

        // $user = Users::findFirst('username="' .  $request->getPost('username').'" AND  password="'. sha1($request->getPost('password')).'" AND status=1');

         $user = Users::findFirst('username="'.$username.'" AND  password="'. sha1($password).'" AND status=1');////CURL*******
        if($user) {
            $jwt = new \Security\Jwt\JWT();
            $payload = array(
                "id" => $user->id,
                "username" => $user->username,
                "lastname" => $user->last_name,
                "firstname" => $user->first_name,
                "exp" => time() + (60 * 60)); // time in the future
            $token = $jwt->encode($payload, $app->config->hashkey);
            // echo json_encode(array('data' => $token));

            echo json_encode(array('success'=>$payload)); ////CURL*******
        }else{
            echo json_encode(array('error' => 'Username or Password is invalid.'));
        }

    }

    public function putaAction() {
        echo "------------------------------";
    }


    public function userListAction($num, $page, $keyword, $id) {
        if ($keyword == null || $keyword == undefined) {
            $userlist = Users::find("id!='" . $id . "'");
        } else {
            $conditions = "username LIKE '%" . $keyword . "%'
            or first_name LIKE '%" . $keyword . "%'
            or last_name LIKE '%" . $keyword . "%' AND id!='" . $id . "'";
            $userlist= Users::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $userlist,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id' => $m->id,
                'fname' => $m->first_name,
                'lname' => $m->last_name,
                'email' => $m->email,
                'username' => $m->username,
                'status' => $m->status,
                'userrole' => $m->task
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }

    public function deleteUserAction($id){
        $dlt = Users::findFirst('id="' . $id . '"');
        if ($dlt) {
            if($dlt->delete()){
                $data = array('msg' => 'User has been successfully Deleted!', 'type'=>'success');
            }else {
                $data = array('type' => 'error', 'msg' => 'Error deleting user.');
            }
        }
        echo json_encode($data);

    }
    public function userInfoction($id) {
        $getInfo = Users::findFirst('id="'. $id .'"');
        $data = array(
            'id' =>  $getInfo->id,
            'username' =>  $getInfo->username,
            'email' =>  $getInfo->email,
            'task' =>  trim($getInfo->task),
            'fname' =>  $getInfo->first_name,
            'lname' =>  $getInfo->last_name,
            'bday' =>  $getInfo->birthday,
            'gender' => $getInfo->gender,
            'status' => $getInfo->status,
            'profile_pic_name' => $getInfo->profile_pic_name
            );
        echo json_encode($data);
    }
    public function userUpdateAction() {
        $request = new \Phalcon\Http\Request();
        //var_dump($request->getPost('username'));
        if($request->isPost()){
            //VARIABLE
            $id         = $request->getPost('id');
            $username   = $request->getPost('username');
            $email      = $request->getPost('email');
            $password   = $request->getPost('password');
            if(isset($_POST['userrole'])){$userrole   = $request->getPost('userrole');}
            $firstname  = ucwords(strtolower($request->getPost('fname')));
            $lastname   = ucwords(strtolower($request->getPost('lname')));
            $bday       = $request->getPost('bday');
            $gender     = $request->getPost('gender');
            $status     = $request->getPost('status');
            $profile_pic_name     = $request->getPost('profile_pic_name');
            if(isset($_POST['password'])){$password = $request->getPost('password');}

            if($status == "true"){
                $status = 1;
            }else {
                $status = 0;
            }

            //SAVE
            $usave = Users::findFirst('id="' . $id . '"');
            $usave->username        = $username;
            $usave->email           = $email;
            if(isset($_POST['userrole'])){$usave->task = $userrole;}
            $usave->first_name      = $firstname;
            $usave->last_name       = $lastname;
            $usave->birthday        = $bday;
            $usave->gender          = $gender;
            $usave->status          = $status;
            $usave->profile_pic_name= $profile_pic_name;
            if(isset($_POST['password'])){$usave->password = sha1($password);}

            if(!$usave->save()){
                $errors = array();
                foreach ($usave->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('type' => 'danger', 'msg' => $errors));
            }else{
                echo json_encode(array('type' => 'success', 'msg' => 'User info has been successfuly updated.'));
            }
        }
    }

    public function resetpasswordAction(){

        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $email = $request->getPost('email');
            $data = array();
            $NMSemail = $email;
            $user = Users::findFirst("email='" . $email . "'");
            if($user == true){
                $a = '';
                for ($i = 0; $i < 6; $i++) {
                    $a .= mt_rand(0, 9);
                }
                $token = sha1($a);

                $forgotEmail = Forgotpassword::findFirst("email='" . $email . "'");
                if ($forgotEmail == true)
                {
                    $forgotEmail->delete();
                }
                $forgotpassword = new Forgotpassword();
                $forgotpassword->assign(array(
                    'email' => $email,
                    'token' => $token,
                    'date' => date('Y-m-d H:i:s')
                    ));
                if (!$forgotpassword->save())
                {
                    $type="danger";
                    $msg = "Something went wrong saving the data, please try again.";
                }
                else
                {
                    $dc = new CB();
                    $body = '<div style="background-color: #eee;padding:20px;margin-bottom:10px;">You&#39;re receiving this e-mail because you requested a password reset for your account at Sedona Healing Arts website.
                    <br>
                    <br>
                    Please click the Reset Password link and choose a new password:
                    <br>
                    <a href="'.$GLOBALS["baseURL"].'/sedonaadmin/forgotpassword/changepassword/'.$NMSemail.'/'.$token.'" target="_blank">Reset Password</a>
                    <br>
                    <br>
                    <br></div>';

                    $send = $dc->sendMail($NMSemail,'Sedona Healing Arts : Password Reset',$body);
                    $type="success";
                    $msg = "Password Reset has been sent to ". $NMSemail .". Please check your inbox.";
                }

            }else {
                $type= "danger";
                $msg = "Invalid email address";
            }

            echo json_encode(array('type' => $type, 'msg' => $msg));
        }
    }

    public function changepasswordAction($email, $token){
        $forgotpassword = Forgotpassword::findFirst('email="'. $email.'" AND token="'.$token.'"');
        if($forgotpassword){
            $data = array('valid' => true  );
        }else {
            $data = array('valid' => false );
        }
        echo json_encode($data);
    }

    public function updatepasswordAction(){
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $password = sha1($request->getPost('password'));
            $email = $request->getPost('email');
            $token = $request->getPost('token');

            $update = Users::findFirst("email='" . $email . "'");
            if($update){
                $update->password = $password;
                $update->updated_at = date('Y-m-d H:i:s');
                if($update->save()){
                    $delete = Forgotpassword::findFirst('email="'. $email.'" AND token="'.$token.'"');
                    $delete->delete();

                    $DI = \Phalcon\DI::getDefault();
                    $app = $DI->get('application');

                    $type="success";
                    $msg = "Your password has been successfully updated. You can now <a href='".$GLOBALS["baseURL"]."/sedonaadmin' class='login'>log in</a>";
                }else {
                    $type="danger";
                    $msg = "Error updating password";
                }
            }else {
                $type="danger";
                $msg = "Error updating password";
            }

            echo json_encode(array('type' => $type, 'msg' => $msg ));
        }
    }

    public function updatestatusAction($id, $status){
        $user = Users::findFirst("id='" . $id . "'");
        $user->status = $status;
        $user->save();
    }
}

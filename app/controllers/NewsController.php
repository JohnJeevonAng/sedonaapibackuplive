<?php

namespace Controllers;

use \Models\News as News;
use \Models\Newsimage as Newsimage;
use \Models\Author as Author;
use \Models\Authorimage as Authorimage;
use \Models\Newscategory as Newscategory;
use \Models\Newstags as Newstags;
use \Models\Newscat as Newscat;
use \Models\Tags as Tags;
use \Models\Newsvideo as Newsvideo;
use \Models\Featuredvideo as Featuredvideo;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class NewsController extends \Phalcon\Mvc\Controller {

    public function createNewsAction() {
        // var_dump($_POST);

        $request = new \Phalcon\Http\Request();

        if($request->isPost()){


            $title = $request->getPost('title');
            $slugs = $request->getPost('slugs');
            $author = $request->getPost('author');
            $date = $request->getPost('date');
            $summary = $request->getPost('summary');
            $body = $request->getPost('body');
            $videothumb = '';
            $imagethumb = '';
            if($request->getPost('featuredthumbtype')=='video'){
                $videothumb = $request->getPost('featuredthumb');
            }elseif($request->getPost('featuredthumbtype')=='image'){
                $imagethumb = $request->getPost('featuredthumb');
            }
            $status = $request->getPost('status');
            $category = $request->getPost('category');
            $tags = $request->getPost('tag');
            $metatitle = $request->getPost('metatitle');
            $metakeyword = $request->getPost('metakeyword');
            $metadesc = $request->getPost('metadesc');

            $findNews = News::findFirst('title="'.$title.'"');
            if($findNews){
                die(json_encode(array('error' => array('existTitle' => 'Title is already existing'))));
            }



            $guid = new \Utilities\Guid\Guid();
            $newsid = $guid->GUID();

            $page = new News();
            $page->assign(array(
                'newsid' => $newsid,
                'title' => $title,
                'newsslugs' => $slugs,
                'summary' => $summary,
                'author' => $author,
                'body' => $body,
                'imagethumb' => $imagethumb,
                'videothumb' => $videothumb,
                'date' => $date,
                'status' => $status,
                'views' => 1,
                'type' => 'News',
                'featurednews' => 0,
                'datecreated' => date("Y-m-d H:i:s"),
                'dateedited' =>date("Y-m-d H:i:s"),
                'metatitle' => $metatitle,
                'metakeyword' => $metakeyword,
                'metadesc' => $metadesc
                ));

            if (!$page->save()) {
                $errors = array();
                foreach ($page->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {

                foreach($category as $cat){
                    $catnews = new Newscat();
                    $catnews->assign(array(
                        'id' => $guid->GUID(),
                        'newsid' => $newsid,
                        'catid' => $cat
                    ));
                    if (!$catnews->save())
                    {
                        $errors = array();
                        foreach ($page->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        echo json_encode(array('error' => $errors));
                    }
                    else
                    {
                        $data['success'] = "Success cat";
                    }
                }

                $newstags = $tags;
                foreach($newstags as $nt){
                    $gettags = Newstags::findFirst("tags='$nt'");
                    if(!$gettags){
                        var_dump($nt);
                        $newstags = new Newstags();
                        $newstags->assign(array(
                            'newsid' => $newsid,
                            'tags' => $nt,
                            'slugs' => str_replace("-", " ", $nt)
                        ));

                        if (!$newstags->save())
                        {
                            $data['error'] = "Something went wrong saving the newstags, please try again.";
                        }
                        else
                        {
                            $tagsofnews = new Tags();
                            $tagsofnews->assign(array(
                                'newsid' => $newsid,
                                'tags' => $newstags->id
                            ));
                            if (!$tagsofnews->save())
                            {
                                $data['error'] = "Something went wrong saving the tags, please try again.";
                            }
                            else
                            {
                                $data['success'] = "Success Tags";
                            }
                        }

                    }else{
                        $data['success'] = "Success";
                        $tagsofnews = new Tags();
                        $tagsofnews->assign(array(
                            'newsid' => $newsid,
                            'tags' => $gettags->id
                        ));
                        if (!$tagsofnews->save())
                        {
                            $data['error'] = "Something went wrong saving the newstags, please try again.";
                        }
                        else
                        {
                            $data['success'] = "Success newstags else";
                        }
                    }

                }


            }

        }

        echo json_encode($data);

    }


    public function saveimageAction() {
        $filename = $_POST['imgfilename'];
        $picture = new Newsimage();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }

    }

    public function listimageAction() {

        $getimages = Newsimage::find(array("order" => "id DESC"));
        foreach ($getimages as $getimages)
        {
            $data[] = array(
                'id'=>$getimages->id,
                'filename'=>$getimages->filename
                );
        }
        echo json_encode($data);

    }


    public function savevideoAction() {
        $video = $_POST['newsvid'];

        $guid = new \Utilities\Guid\Guid();
        $videoid = $guid->GUID();

        $vid = new Newsvideo();
        $vid->assign(array(
            'videoid' => $videoid,
            'video' => $video
            ));

        if (!$vid->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }

        echo json_encode($data);

    }

    public function listvideoAction() {

        $getvid = Newsvideo::find(array("order" => "num DESC"));
        foreach ($getvid as $getvid)
        {
            $data[] = array(
                'videoid'=>$getvid->videoid,
                'video'=>$getvid->video
                );
        }
        echo json_encode($data);

    }



    public function listcategoryAction()
    {

        $getcategory= Newscategory::find(array("order" => "categoryid ASC"));
        foreach ($getcategory as $getcategory) {
            $data[] = array(
                'categoryid'=>$getcategory->categoryid,
                'categoryname'=>$getcategory->categoryname
                );
        }
        echo json_encode($data);

    }

    public function listtagsAction()
    {

        $getcategory= Newstags::find(array("order" => "id ASC"));
        foreach ($getcategory as $getcategory) {
            $data[] = array(
                'id'=>$getcategory->id,
                'tags'=>$getcategory->tags,
                'slugs'=>$getcategory->slugs
                );
        }
        echo json_encode($data);

    }

    public function listAuthorAction()
    {

        $getAuthor= Author::find(array("order" => "authorid ASC"));
        foreach ($getAuthor as $getAuthor) {
            $data[] = array(
                'authorid'=>$getAuthor->authorid,
                'name'=>$getAuthor->name,
                'about'=>$getAuthor->about,
                'image'=>$getAuthor->image
                );
        }
        echo json_encode($data);

    }


    public function manageNewsAction($num, $page, $keyword, $sort) {

//        if ($keyword == 'null' || $keyword == 'undefined') {
//           $offsetfinal = ($page * 10) - 10;
//
//           $db = \Phalcon\DI::getDefault()->get('db');
//           $stmt = $db->prepare("SELECT * FROM news ORDER BY dateedited DESC  LIMIT " . $offsetfinal . ",10");
//
//           $stmt->execute();
//           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
//
//
//           $db1 = \Phalcon\DI::getDefault()->get('db');
//           $stmt1 = $db1->prepare("SELECT * FROM news ORDER BY dateedited DESC");
//
//           $stmt1->execute();
//           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);
//
//           $totalreportdirty = count($searchresult1);
//        } else {
//
//         $offsetfinal = ($page * 10) - 10;
//
//         $db = \Phalcon\DI::getDefault()->get('db');
//         $stmt = $db->prepare("SELECT * FROM news WHERE news.title LIKE '%" . $keyword . "%' or news.newsslugs LIKE '%" . $keyword . "' ORDER BY dateedited DESC LIMIT " . $offsetfinal . ",10");
//
//         $stmt->execute();
//         $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
//
//
//         $db1 = \Phalcon\DI::getDefault()->get('db');
//         $stmt1 = $db1->prepare("SELECT * FROM news WHERE news.title LIKE '%" . $keyword . "%' or news.newsslugs LIKE '%" . $keyword . "' ORDER BY dateedited DESC LIMIT " . $offsetfinal . ",10");
//
//         $stmt1->execute();
//         $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);
//
//         $totalreportdirty = count($searchresult1);
//
//        }


        $app = new CB();

        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT * FROM news INNER JOIN author ON author.authorid = news.author';
        $sqlCount = 'SELECT COUNT(*) FROM news INNER JOIN author ON author.authorid = news.author';

        if ($keyword != 'null' && $keyword != 'undefined') {
            $sqlconcat = " WHERE news.title LIKE '%" . $keyword . "%' OR author.name LIKE '%".$keyword."%' ";
            $sql .= $sqlconcat;
            $sqlCount .= $sqlconcat;
        }

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }
        if($sort == "date" || $sort == "datecreated" || $sort == "dateedited"){
            $sql .= " ORDER BY ".$sort." DESC ";
        }else {
            $sql .= " ORDER BY ".$sort." ASC ";
        }
        
        $sql .= " LIMIT " . $offsetfinal . ",10";
        // getting the query
        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);

        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));
    }


    public function newsUpdatestatusAction($status,$newsid,$keyword) {

        $data = array();
        $news = News::findFirst('newsid="' . $newsid . '"');
        $news->status = $status;
            if (!$news->save()) {
                $news['error'] = "Something went wrong saving page status, please try again.";
            } else {
                $data['success'] = "Success";
            }

            echo json_encode($data);
    }



    public function newsdeleteAction($newsid) {
        $conditions = 'newsid="' . $newsid . '"';
        $news = News::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($news) {
            if ($news->delete()) {
                $data = array('success' => 'News Deleted');
            }
        }
        echo json_encode($data);
    }



    public function newseditoAction($newsid) {
        $data = array();

        $news = News::findFirst('newsid="' . $newsid . '"');
        if ($news) {
            $db1 = \Phalcon\DI::getDefault()->get('db');
            $stmt1 = $db1->prepare("SELECT id, newstags.tags FROM newstags INNER JOIN tags ON newstags.id = tags.tags WHERE tags.newsid = '" . $newsid . "'");

            $stmt1->execute();
            $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

            $db2 = \Phalcon\DI::getDefault()->get('db');
            $stmt2 = $db2->prepare("SELECT categoryid, categoryname FROM newscat INNER JOIN newscategory ON newscat.catid = newscategory.categoryid WHERE newscat.newsid = '" . $newsid . "'");

            $stmt2->execute();
            $searchresult2 = $stmt2->fetchAll(\PDO::FETCH_ASSOC);


            $data = array(
                'newsid' => $news->newsid,
                'title' => $news->title,
                'slugs' => $news->newsslugs,
                'author' => $news->author,
                'summary' => $news->summary,
                'body' => $news->body,
                'imagethumb' => $news->imagethumb,
                'videothumb' => $news->videothumb,
                'newslocation' => $news->newslocation,
                'category' => $searchresult2,
                'status' => $news->status,
                'featurednews' => $news->featurednews,
                'date' => $news->date,
                'datecreated' => $news->datecreated,
                'tag' => $searchresult1,
                'metatitle' => $news->metatitle,
                'metakeyword' => $news->metakeyword,
                'metadesc' => $news->metadesc
                );
        }
        echo json_encode($data);
    }



    public function updateNewsAction() {



        $request = new \Phalcon\Http\Request();

        if($request->isPost()){



            $newsid = $request->getPost('newsid');
            $title = $request->getPost('title');
            $slugs = $request->getPost('slugs');
            $author = $request->getPost('author');
            $date = $request->getPost('date');
            $summary = $request->getPost('summary');
            $body = $request->getPost('body');
            $videothumb = '';
            $imagethumb = '';
            if($request->getPost('featuredthumbtype')=='video'){
                $videothumb = $request->getPost('featuredthumb');
            }elseif($request->getPost('featuredthumbtype')=='image'){
                $imagethumb = $request->getPost('featuredthumb');
            }
            $newslocation = $request->getPost('newslocation');
            $category = $request->getPost('category');
            // $featurednews = $request->getPost('featurednews');
            // $datecreated = $request->getPost('datecreated');
            $status = $request->getPost('status');
            $tags = $request->getPost('tag');
            $metatitle = $request->getPost('metatitle');
            $metakeyword = $request->getPost('metakeyword');
            $metadesc = $request->getPost('metadesc');

            if($imagethumb != ""){
                $videothumb = "";
            }else if($videothumb != ""){
                $imagethumb = "";
            }

            // $conditions = 'newsid="' . $newsid . '"';
            // $deletenews = News::findFirst(array($conditions));
            // $data = array('error' => 'Not Found');
            // if ($deletenews) {
            //     if ($deletenews->delete()) {
            //         $data = array('success' => 'News Deleted');
            //     }
            // }

            $findNews = News::findFirst('title="'.$title.'" AND newsid!="'.$newsid.'"');
            if($findNews){
                die(json_encode(array('error' => array('existTitle' => 'Title is already existing'))));
            }

            $news = News::findFirst('newsid="' . $newsid . '"');
            $news->title = $title;
            $news->newsslugs = $slugs;
            $news->author = $author;
            $news->summary = $summary;
            $news->body = $body;
            $news->imagethumb = $imagethumb;
            $news->videothumb = $videothumb;
            $news->newslocation = $newslocation;
            $news->date = $date;
            $news->dateedited = date('Y-m-d');
            $news->metatitle = $metatitle;
            $news->metakeyword = $metakeyword;
            $news->metadesc = $metadesc;
            $news->status = $status;

            // $page = new News();
            // $page->assign(array(
            //     'newsid' => $newsid,
            //     'title' => $title,
            //     'newsslugs' => $slugs,
            //     'author' => $author,
            //     'summary' => $summary,
            //     'body' => $body,
            //     'imagethumb' => $imagethumb,
            //     'videothumb' => $videothumb,
            //     'newslocation' => $newslocation,
            //     'category' => $category,
            //     'date' => $d,
            //     'status' => $status,
            //     'featurednews' => $featurednews,
            //     'views' => 1,
            //     'type' => 'News',
            //     'datecreated' => $datecreated,
            //     'dateedited' =>date('Y-m-d')
            //     ));

            if (!$news->save()) {
                $errors = array();
                foreach ($news->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {

                $conditions = 'newsid="' . $newsid . '"';
                $deletenewstags = Tags::find(array($conditions));
                $data = array('error' => 'Not Found');
                if ($deletenewstags) {
                    foreach($deletenewstags as $deletenewstags){
                        if ($deletenewstags->delete()) {
                            $data = array('success' => 'tags Deleted');
                        }
                    }
                }

                $conditions = 'newsid="' . $newsid . '"';
                $deletenewscats = Newscat::find(array($conditions));
                $data = array('error' => 'Not Found');
                if ($deletenewscats) {
                    foreach($deletenewscats as $deletenewscats){
                        if ($deletenewscats->delete()) {
                            $data = array('success' => 'Categories Deleted');
                        }
                    }
                }
                $guid = new \Utilities\Guid\Guid();
                foreach($category as $cat){
                    $catnews = new Newscat();
                    $catnews->assign(array(
                        'id' => $guid->GUID(),
                        'newsid' => $newsid,
                        'catid' => $cat
                    ));
                    if (!$catnews->save())
                    {
                        $errors = array();
                        foreach ($page->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        echo json_encode(array('error' => $errors));
                    }
                    else
                    {
                        $data['success'] = "Success cat";
                    }
                }

                $newstags = $tags;
                foreach($newstags as $nt){
                    $gettags = Newstags::findFirst("tags='$nt'");
                    if(!$gettags){
                        var_dump($nt);
                        $newstags = new Newstags();
                        $newstags->assign(array(
                            'newsid' => $newsid,
                            'tags' => $nt,
                            'slugs' => str_replace("-", " ", $nt)
                        ));

                        if (!$newstags->save())
                        {
                            $data['error'] = "Something went wrong saving the newstags, please try again.";
                        }
                        else
                        {
                            $tagsofnews = new Tags();
                            $tagsofnews->assign(array(
                                'newsid' => $newsid,
                                'tags' => $newstags->id
                            ));
                            if (!$tagsofnews->save())
                            {
                                $data['error'] = "Something went wrong saving the tags, please try again.";
                            }
                            else
                            {
                                $data['success'] = "Success Tags";
                            }
                        }

                    }else{
                        $data['success'] = "Success";
                        $tagsofnews = new Tags();
                        $tagsofnews->assign(array(
                            'newsid' => $newsid,
                            'tags' => $gettags->id
                        ));
                        if (!$tagsofnews->save())
                        {
                            $data['error'] = "Something went wrong saving the newstags, please try again.";
                        }
                        else
                        {
                            $data['success'] = "Success newstags else";
                        }
                    }

                }
            }

        }

        echo json_encode($data);



    }





    public function managecategoryAction($num, $page, $keyword) {

        if ($keyword == 'null' || $keyword == 'undefined') {
            $Pages = Newscategory::find();
        } else {
            $conditions = "categoryname LIKE '%" . $keyword . "%'";
            $Pages = Newscategory::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $Pages,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'categoryid' => $m->categoryid,
                'categoryname' => $m->categoryname,
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

    }

    public function managetagsAction($num, $page, $keyword) {

        if ($keyword == 'null' || $keyword == 'undefined') {
            $tag = Newstags::find();
        } else {
            $conditions = "tags LIKE '%" . $keyword . "%'";
            $tag = Newstags::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $tag,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id' => $m->id,
                'tags' => $m->tags,
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

    }

    public function createcategoryAction()
    {

        $findNewsCategory = Newscategory::findFirst('categoryname = "'.$_POST['catnames'].'"');
        if($findNewsCategory){
            die(json_encode(array('error' => array('existCategory' => 'Category is already existing'))));
        }

        $data = array();

       $catnames = new Newscategory();
       $catnames->assign(array(
        'categoryname' => $_POST['catnames'],
        'categoryslugs' => $_POST['slugs'],
        ));
        if (!$catnames->save()){
        $errors = array();
                foreach ($catnames->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
        }
        else{
         $data['success'] = "Success";
        }
        echo json_encode($data);
    }


    /* Add Tags */
    public function createtagsAction()
    {

        $findNewstags = Newstags::findFirst('tags="'.$_POST['tags'].'"');
        if($findNewstags){
            die(json_encode(array('error' => array('existTags' => 'Tag is already existing'))));
        }

       $data = array();

       $tagsnames = new Newstags();
       $tagsnames->assign(array(
        'tags' => $_POST['tags'],
        'slugs' => rawurlencode(str_replace("%20", " ", $_POST['tags'])),
        ));
        if (!$tagsnames->save()){
        $errors = array();
                foreach ($tagsnames->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
        }
        else{
         $data['success'] = "Success";
         $data['data'] = $tagsnames;
        }
        echo json_encode($data);
    }

    public function updatecategoryAction($catname,$id)
    {

        $find = Newscategory::findFirst('categoryid!=' . $id . ' AND categoryname="'.$catname.'" ');
        if($find){
            $data['error'] = "Your data already exist.";
            echo json_encode($data);
        }else{
            $data = array();
            $news = Newscategory::findFirst('categoryid=' . $id . '');
            $news->categoryname = $catname;
            $news->categoryslugs = rawurlencode(str_replace('%20', ' ', $catname));
            if (!$news->save())
            {
                $data['error'] = "Something went wrong saving the data, please try again.";
            }
            else
            {
                $data['success'] = "Success";
            }
            echo json_encode($data);
        }
    }

    public function updatetagsAction($tagname,$id)
    {

        $find = Newstags::findFirst('id!=' . $id . ' AND tags = "'.$tagname.'"');
        if($find){
            $data['error'] = "Your data already exist.";
            echo json_encode($data);
        }else {
            $data = array();
            $news = Newstags::findFirst('id=' . $id . ' ');
            $news->tags = $tagname;
            $news->slugs = rawurlencode(str_replace('%20', ' ', $tagname));

            if (!$news->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }
            echo json_encode($data);
        }
    }

    public function categorydeleteAction($id) {
        $conditions = "categoryid=" . $id;
        $news = Newscategory::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($news) {
            if ($news->delete()) {
                $data = array('success' => 'Category Deleted');
            }
        }
        echo json_encode($data);
    }

    public function tagsdeleteAction($id) {
        $conditions = "id=" . $id;
        $news = Newstags::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($news) {
            if ($news->delete()) {
                $data = array('success' => 'Category Deleted');
            }
        }
        echo json_encode($data);
    }

    public function listcategoryfrontendAction() {

    $newscategory = Newscategory::find(array());
    $newscategory = json_encode($newscategory->toArray(), JSON_NUMERIC_CHECK);
    echo $newscategory;

    }

    public  function listnewsbytagAction($tag ,$offset, $page){
        $app = new CB();
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM news INNER JOIN tags ON news.newsid = tags.newsid INNER JOIN newstags ON newstags.id = tags.tags LEFT JOIN newscategory ON news.category=newscategory.categoryid LEFT JOIN author ON news.author=author.authorid WHERE slugs = '". $tag ."' and status = 1 ORDER BY date DESC LIMIT ".$offset.','. $page);

        $stmt->execute();
        $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach($searchresult as $sr=>$val){
            $categories = $app->dbSelect("SELECT newscategory.categoryid, newscategory.categoryname, newscategory.categoryslugs FROM newscategory INNER JOIN newscat ON newscategory.categoryid = newscat.catid WHERE newscat.newsid ='".$val['newsid']."'");
            $searchresult[$sr]['category'] = $categories;
        }
        echo json_encode($searchresult);
    }


    // public function featurednewsfrontendAction() {

    //     $db = \Phalcon\DI::getDefault()->get('db');
    //      $stmt = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 1 and status = 1 LIMIT 0, 1");

    //      $stmt->execute();
    //      $featurednews = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    //      echo json_encode($featurednews);

    // }

    public function latestnewsfrontendAction() {
        $app = new CB();
        $db = \Phalcon\DI::getDefault()->get('db');
         $stmt = $db->prepare("SELECT * FROM news LEFT JOIN newscategory ON news.category=newscategory.categoryid LEFT JOIN author ON news.author=author.authorid where status=1 ORDER BY date DESC LIMIT 0, 1");

         $stmt->execute();
         $latestnews = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach($latestnews as $sr=>$val){
            $categories = $app->dbSelect("SELECT newscategory.categoryid, newscategory.categoryname, newscategory.categoryslugs FROM newscategory INNER JOIN newscat ON newscategory.categoryid = newscat.catid WHERE newscat.newsid ='".$val['newsid']."'");
            $latestnews[$sr]['categorylist'] = $categories;
        }
         echo json_encode($latestnews);

    }

    // public function founderswisdomfrontendAction() {

    //     $db = \Phalcon\DI::getDefault()->get('db');
    //      $stmt = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE categoryid = 9 and status = 1 ORDER BY date DESC LIMIT 0, 1");

    //      $stmt->execute();
    //      $founderswisdome = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    //      echo json_encode($founderswisdome);

    // }

    public function viewfronentnewsAction($newsslugs) {
        $app = new CB();
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM news LEFT JOIN newscategory ON news.category=newscategory.categoryid LEFT JOIN author ON news.author=author.authorid WHERE newsslugs = '". $newsslugs ."' and status = 1");

        $stmt->execute();
        $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        foreach($searchresult as $sr=>$val){
            $categories = $app->dbSelect("SELECT newscategory.categoryid, newscategory.categoryname, newscategory.categoryslugs FROM newscategory INNER JOIN newscat ON newscategory.categoryid = newscat.catid WHERE newscat.newsid ='".$val['newsid']."'");
            $searchresult[$sr]['categorylist'] = $categories;
        }

        echo json_encode($searchresult);

    }

    public function viewfronenttagsAction($newsslugs) {

        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM tags LEFT JOIN news ON tags.newsid=news.newsid LEFT JOIN newstags ON tags.tags=newstags.id WHERE news.newsslugs = '". $newsslugs ."' ORDER BY dateedited DESC");

        $stmt->execute();
        $newstags = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        echo json_encode($newstags);

    }

    public function listnewsbycategoryAction($category, $offset, $page) {
        $app = new CB();
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM newscat INNER JOIN newscategory ON newscategory.categoryid = newscat.catid INNER JOIN news ON news.newsid=newscat.newsid LEFT JOIN author ON news.author=author.authorid WHERE newscategory.categoryslugs = '". $category ."' and status = 1 ORDER BY date DESC LIMIT ".$offset.','. $page);

        $stmt->execute();
        $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        foreach($searchresult as $sr=>$val){
            $categories = $app->dbSelect("SELECT newscategory.categoryid, newscategory.categoryname FROM newscategory INNER JOIN newscat ON newscategory.categoryid = newscat.catid WHERE newscat.newsid ='".$val['newsid']."'");
            $searchresult[$sr]['categorylist'] = $categories;
        }

        echo json_encode($searchresult);

    }

    //list all news
    public function listAction($offset,$page) {
        $app = new CB();

        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM news LEFT JOIN author ON news.author=author.authorid WHERE status = 1 ORDER BY date DESC LIMIT $offset, $page");

        $stmt->execute();
        $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        foreach($searchresult as $sr=>$val){
            $categories = $app->dbSelect("SELECT newscategory.categoryid, newscategory.categoryname FROM newscategory INNER JOIN newscat ON newscategory.categoryid = newscat.catid WHERE newscat.newsid ='".$val['newsid']."' ");
            $searchresult[$sr]['category'] = $categories;
        }
        echo json_encode($searchresult);

    }

    //Get author
    public function authorAction($id, $offset, $page) {
        $app = new CB();
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM news LEFT JOIN newscategory ON news.category=newscategory.categoryid LEFT JOIN author ON news.author=author.authorid WHERE author.name = '$id' AND status=1 ORDER BY date DESC LIMIT $offset, $page");

        $stmt->execute();
        $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        foreach($searchresult as $sr=>$val){
            $categories = $app->dbSelect("SELECT newscategory.categoryid, newscategory.categoryname FROM newscategory INNER JOIN newscat ON newscategory.categoryid = newscat.catid WHERE newscat.newsid ='".$val['newsid']."' ");
            $searchresult[$sr]['category'] = $categories;
        }

        echo json_encode($searchresult);

    }

    public function deletenewsimgAction()
    {
        $conditions = 'filename="' . $_POST['imgfilename'] . '"';
        $newsimg = Newsimage::findFirst(array($conditions));
        if ($newsimg) {
            if ($newsimg->delete()) {
                $data = array('success' => 'Image Deleted');
            }
            else
            {
                $data = array('error' => 'Image Not Deleted');
            }
        }
        echo json_encode($data);
    }

    public function deletevideoAction()
    {
        $conditions = 'videoid="' . $_POST['videoid'] . '"';
        $newsvid = Newsvideo::findFirst(array($conditions));
        if ($newsvid) {
            if ($newsvid->delete()) {
                $data = array('success' => 'Image Deleted');
            }
            else
            {
                $data = array('error' => 'Image Not Deleted');
            }
        }
        echo json_encode($data);
    }



    public function authorlistimagesAction() {

        $getimages = Authorimage::find(array("order" => "id DESC"));
        foreach ($getimages as $getimages)
        {
            $data[] = array(
                'id'=>$getimages->authorid,
                'filename'=>$getimages->filename
                );
        }
        echo json_encode($data);

    }

    public function deleteauthorimgAction()
    {
        $conditions = 'filename="' . $_POST['imgfilename'] . '"';
        $newsimg = Authorimage::findFirst(array($conditions));
        if ($newsimg) {
            if ($newsimg->delete()) {
                $data = array('success' => 'Image Deleted');
            }
            else
            {
                $data = array('error' => 'Image Not Deleted');
            }
        }
        echo json_encode($data);
    }

    public function saveauthorimageAction() {
        $filename = $_POST['imgfilename'];
        $picture = new Authorimage();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }

    }

    public function createAuthorAction() {

        $request = new \Phalcon\Http\Request();

        if($request->isPost()){


            $name = $request->getPost('name');
            $about = $request->getPost('about');
            $location = $request->getPost('location');
            $occupation = $request->getPost('occupation');
            $photo = $request->getPost('photo');


            $guid = new \Utilities\Guid\Guid();
            $id = $guid->GUID();

            $author = new Author();
            $author->assign(array(
                'authorid' => $id,
                'name' => $name,
                'location' => $location,
                'occupation' => $occupation,
                'about' => $about,
                'image' => $photo,
                'date_created' => date('Y-m-d'),
                'date_updated' => date('Y-m-d')
                ));

            if (!$author->save()) {
                $errors = array();
                foreach ($author->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                $data['success'] = "Success";

            }

        }

        echo json_encode($data);



    }

    public function manageAuthorAction($num, $page, $keyword) {


        $app = new CB();

        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT * FROM author';
        $sqlCount = 'SELECT COUNT(*) FROM author';

        if ($keyword != 'null' && $keyword != 'undefined') {
            $sqlconcat = " WHERE name LIKE '%" . $keyword . "%' OR location LIKE '%" . $keyword . "%' OR occupation LIKE '%" . $keyword . "%' OR date_created LIKE '%" . $keyword . "%'";;
            $sql .= $sqlconcat;
            $sqlCount .= $sqlconcat;
        }

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }
        $sql .= " ORDER BY name ASC ";
        $sql .= " LIMIT " . $offsetfinal . ",10";
        // getting the query
        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);

        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));
//
//        if ($keyword == 'null' || $keyword == 'undefined') {
//            $Author = Author::find();
//        } else {
//            $conditions = "name LIKE '%" . $keyword . "%' OR location LIKE '%" . $keyword . "%' OR occupation LIKE '%" . $keyword . "%' OR date LIKE '%" . $keyword . "%'";
//            $Author = Author::find(array($conditions));
//        }
//
//        $currentPage = (int) ($page);
//
//        // Create a Model paginator, show 10 rows by page starting from $currentPage
//        $paginator = new \Phalcon\Paginator\Adapter\Model(
//            array(
//                "data" => $Author,
//                "limit" => 10,
//                "page" => $currentPage
//                )
//            );
//
//        // Get the paginated results
//        $page = $paginator->getPaginate();
//
//        $data = array();
//        foreach ($page->items as $m) {
//            $data[] = array(
//                'authorid' => $m->authorid,
//                'name' => $m->name,
//                'location' => $m->location,
//                'occupation' => $m->occupation,
//                'about' => $m->about,
//                'date_created' => $m->date_created,
//                );
//        }
//        $p = array();
//        for ($x = 1; $x <= $page->total_pages; $x++) {
//            $p[] = array('num' => $x, 'link' => 'page');
//        }
//        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

    }

    public function authoreditoAction($authorid) {
        $data = array();

        $author = Author::findFirst('authorid="' . $authorid . '"');
        if ($author) {
            $data = array(
                'authorid' => $author->authorid,
                'name' => $author->name,
                'location' => $author->location,
                'occupation' => $author->occupation,
                'about' => $author->about,
                'photo' => $author->image,
                'date_created' => $author->date_created

                );
        }
        echo json_encode($data);
    }

    public function authorupdateAction() {

       $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $authorid = $request->getPost('authorid');
            $name = $request->getPost('name');
            $location = $request->getPost('location');
            $occupation = $request->getPost('occupation');
            $about = $request->getPost('about');
            $photo = $request->getPost('photo');

            $author = Author::findFirst('authorid="' . $authorid . '"');
            $author->name = $name;
            $author->location = $location;
            $author->occupation = $occupation;
            $author->about = $about;
            $author->image = $photo;

            if (!$author->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";



            }
            echo json_encode($data);



        }
    }

    public function authordeleteAction($authorid) {

        $conditions = 'authorid="' . $authorid . '"';
        $author = Author::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($author) {
            if ($author->delete()) {
                $data = array('success' => 'Author Deleted');
            }
        }
        echo json_encode($data);
    }



    public function featuredvideoAction() {
        var_dump($_POST);
        $video = $_POST['video'];
        $data = array();
        $videotbl = Featuredvideo::findFirst('id=1');
        $videotbl->video = $video;
            if (!$videotbl->save()) {
                $videotbl['error'] = "Something went wrong saving the video, please try again.";
            } else {
                $data['success'] = "Success";
            }

            echo json_encode($data);
    }



    public function displayvideoAction()
    {

        $getVideo= Featuredvideo::find(array("order" => "id ASC"));
        foreach ($getVideo as $getVideo) {
            $data[] = array(
                'video'=>$getVideo->video
                );
        }
        echo json_encode($data);

    }

    public function loadconflictsAction($id) {
        $app = new CB();
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT news.title, newscat.id, author.name FROM newscat INNER JOIN news ON newscat.newsid = news.newsid INNER JOIN author ON news.author = author.authorid WHERE newscat.catid='$id'");

        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $stmt = $db->prepare("SELECT categoryid, categoryname from newscategory WHERE categoryid != $id ");

        $stmt->execute();
        $categoryarray = $stmt->fetchAll(\PDO::FETCH_ASSOC);


        echo json_encode(array("dataconflict" => $data, "newscategory" => $categoryarray));
    }

    public function updateconflictAction() {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $id = $request->getPost('id');
            $catid = $request->getPost('catid');

            $newscat = Newscat::findFirst('id="'.$id.'"');
            $newscat->catid = $catid;
            $newscat->save();
        }
    }

    public function listarchiveAction() {
        $news = News::find(array('status' => 1, 'order' => 'date DESC'));
        $dates = [];
        foreach ($news as $news)
        {

            if(!in_array(date('F Y', strtotime($news->date)), $dates)){
                $data[] = array(
                    'month' => date('F', strtotime($news->date)),
                    'year' => date('Y', strtotime($news->date))
                    );
                array_push($dates, date('F Y', strtotime($news->date)));
            }
        }
        echo json_encode($data);
    }

    public function listnewsbyarchiveAction($month, $year ,$offset, $page) {
        $searchdate = date("Y-m-d", strtotime($month." ".$year));
        $betweendate = date("Y-m-d", strtotime("+1 months -1 day", strtotime($searchdate)));
        $app = new CB();
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM news LEFT JOIN author on news.author = author.authorid WHERE news.date >= '$searchdate' AND news.date <= '$betweendate' AND status = 1 ORDER BY news.date DESC LIMIT ".$offset.','. $page);

        $stmt->execute();
        $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach($searchresult as $sr=>$val){
            $categories = $app->dbSelect("SELECT newscategory.categoryid, newscategory.categoryname, newscategory.categoryslugs FROM newscategory INNER JOIN newscat ON newscategory.categoryid = newscat.catid WHERE newscat.newsid ='".$val['newsid']."'");
            $searchresult[$sr]['category'] = $categories;
        }
        echo json_encode($searchresult);
    }

    public function loadauthornewsAction($authorid){
        $app = new CB();
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM news LEFT JOIN author on news.author = author.authorid WHERE news.author='$authorid'");

        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $stmt = $db->prepare("SELECT * FROM author WHERE authorid != '$authorid' ");

        $stmt->execute();
        $authorarray = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        echo json_encode(array("dataconflict" => $data, "newsauthors" => $authorarray));
    }

    public function updatenewsauthorsAction() {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $newsid = $request->getPost('newsid');
            $author = $request->getPost('authorid');

            $news = News::findFirst('newsid="' . $newsid . '"');
            $news->author = $author;
            $news->save();
        }
    }

    public function tagconflictAction($id) {
        $app = new CB();
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT news.newsid, news.title, tags.tags, author.name FROM tags INNER JOIN news ON tags.newsid = news.newsid INNER JOIN author ON news.author = author.authorid WHERE tags.tags=$id");

        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $stmt = $db->prepare("SELECT id, tags from newstags WHERE id != $id ");

        $stmt->execute();
        $tagsarray = $stmt->fetchAll(\PDO::FETCH_ASSOC);


        echo json_encode(array("dataconflict" => $data, "newstags" => $tagsarray));
    }

    public function updateTagConflictAction(){
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $newsid = $request->getPost('newsid');
            $tags = $request->getPost('id');

            $newscat = Tags::findFirst('newsid="'.$newsid.'"');
            $newscat->tags = $tags;
            $newscat->save();
        }
    }
}

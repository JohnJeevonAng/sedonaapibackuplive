<?php

namespace Controllers;

use \Models\Pages as Pages;
use \Models\Pagecategory as Pagecategory;
use \Models\Pageleftbar as Pageleftbar;
use \Models\Pagerightbar as Pagerightbar;
use \Models\Serviceprices as Serviceprices;
use \Models\Pageimage as Pageimage;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class PagesController extends \Phalcon\Mvc\Controller {

    public function createPagesAction() {

        $request = new \Phalcon\Http\Request();
        
        if($request->isPost()){


            $category = $request->getPost('category');
            $title = $request->getPost('title');
            $slugs = $request->getPost('slugs');
            $subtitle1 = $request->getPost('subtitle1');
            $subtitle2 = $request->getPost('subtitle2');
            $buttontitle = $request->getPost('buttontitle');
            $body = $request->getPost('body');
            $peoplesaying = $request->getPost('peoplesaying');
            $serviceprice = $request->getPost('serviceprice');
            $banner = $request->getPost('banner');
            $imagethumb = $request->getPost('imagethumb');
            $layout = $request->getPost('layout');
            $leftbar = $request->getPost('leftbar');
            $rightbar = $request->getPost('rightbar');
            $imagethumbsubtitle = $request->getPost('imagethumbsubtitle');
            $thumbdesc = $request->getPost('thumbdesc');
            $metatitle = $request->getPost('metatitle');
            $metadesc = $request->getPost('metadesc');
            $metatags = preg_replace('/^([^a-zA-Z0-9])*/', '', $request->getPost('metatags'));
            $action = isset($_POST['action']) ? $_POST['action'] : null;
            $guid = new \Utilities\Guid\Guid();
            $id = $guid->GUID();

            $page = new Pages();
            $page->assign(array(
                'pageid' => $id,
                'category' => $category,
                'title' => $title,
                'metatitle' => $metatitle,
                'metadesc' => $metadesc,
                'metatags' => $metatags,
                'pageslugs' => $slugs,
                'subtitle1' => $subtitle1,
                'subtitle2' => $subtitle2,
                'pagecreated_at' => date("Y-m-d H:i:s"),
                'imagethumb' => $imagethumb,
                'buttontitle' => $buttontitle,
                'body' => $body,
                'peoplesaying' => $peoplesaying,
                'serviceprice' => $serviceprice,
                'banner' => $banner,
                'status' => 1,
                'pagelayout' => $layout,
                'type' => 'Pages',
                'imagethumbsubtitle' => $imagethumbsubtitle,
                'thumbdesc' => $thumbdesc,
                'action' => $action
                ));

            if (!$page->save()) {
                $errors = array();
                foreach ($page->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                $data['success'] = "Success";

            }

        }

        echo json_encode($data);



    }

    public function saveimageAction() {

         $filename = $_POST['imgfilename'];

        $picture = new Pageimage();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }
     
    }

    public function listimageAction() {

        $getimages = Pageimage::find(array("order" => "id DESC"));
        foreach ($getimages as $getimages) 
        {
            $data[] = array(
                'id'=>$getimages->id,
                'filename'=>$getimages->filename
                );
        }
        echo json_encode($data);

    }


    public function managepagesAction($num, $page, $keyword) {

//        if ($keyword == 'null' || $keyword == 'undefined') {
//            // $offsetfinal = ($page * 10) - 10;
//
//            // $db = \Phalcon\DI::getDefault()->get('db');
//            // $stmt = $db->prepare("SELECT * FROM pages ORDER BY dateedited DESC  LIMIT " . $offsetfinal . ",10");
//
//            // $stmt->execute();
//            // $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
//
//
//            // $db1 = \Phalcon\DI::getDefault()->get('db');
//            // $stmt1 = $db1->prepare("SELECT * FROM news ORDER BY dateedited DESC");
//
//            // $stmt1->execute();
//            // $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);
//
//            // $totalreportdirty = count($searchresult1);
//
//
//            $Pages = Pages::find();
//        } else {
//            $conditions = "title LIKE '%" . $keyword . "%' OR pageslugs LIKE '%" . $keyword . "%'";
//            $Pages = Pages::find(array($conditions));
//        }
//
//        $currentPage = (int) ($page);
//
//        // Create a Model paginator, show 10 rows by page starting from $currentPage
//        $paginator = new \Phalcon\Paginator\Adapter\Model(
//            array(
//                "data" => $Pages,
//                "limit" => 10,
//                "page" => $currentPage
//                )
//            );
//
//        // Get the paginated results
//        $page = $paginator->getPaginate();
//
//        $data = array();
//        foreach ($page->items as $m) {
//            $cat = Pagecategory::findFirst('id="'.$m->category.'"');
//            $catname = $cat->title;
//            $cdata = array(
//                'pageid' => $m->pageid,
//                'title' => $m->title,
//                'pageslugs' => $m->pageslugs,
//                'status' => $m->status,
//                'category' => $catname
//            );
//            $data[] = $cdata;
//        }
//        $p = array();
//        for ($x = 1; $x <= $page->total_pages; $x++) {
//            $p[] = array('num' => $x, 'link' => 'page');
//        }

        $app = new CB();

        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT pages.title, pages.pageid, pages.banner, pagecategory.title as categorytitle, pages.pageslugs, pages.status FROM pages INNER JOIN pagecategory ON pages.category = pagecategory.id';
        $sqlCount = 'SELECT COUNT(*) FROM pages INNER JOIN pagecategory ON pages.category = pagecategory.id';

        if ($keyword != 'null' && $keyword != 'undefined') {
            $sqlQuery = " WHERE pages.title LIKE '%" . $keyword . "%' OR pages.pageslugs LIKE '%" . $keyword . "' OR pagecategory.title LIKE '%".$keyword."%' ";
            $sql .= $sqlQuery;
            $sqlCount .= $sqlQuery;
        }

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }
        $sql .= " ORDER BY UNIX_TIMESTAMP(pagecreated_at) DESC ";
        $sql .= " LIMIT " . $offsetfinal . ",10";
        // getting the query
        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);

        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));
    }

    public function pageUpdatestatusAction($status,$pageid,$keyword) {

        $data = array();
        $page = Pages::findFirst('pageid="' . $pageid . '"');
        $page->status = $status;
            if (!$page->save()) {
                $data['error'] = "Something went wrong saving page status, please try again.";
            } else {
                $data['success'] = "Success";
            }

            echo json_encode($data);
    }

    public function pagedeleteAction($pageid) {
        $conditions = 'pageid="' . $pageid . '"';
        $pages = Pages::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($pages) {
            if ($pages->delete()) {
                $data = array('success' => 'Page Deleted');
            }
        }
        echo json_encode($data);
    }

    public function pageeditoAction($pageid) {
        $data = array();
        $leftbaritem = array();
        $rightbaritem = array();

        $pages = Pages::findFirst('pageid="' . $pageid . '"');
        $leftbar = Pageleftbar::findFirst('pageid="' . $pageid . '"');
         if ($leftbar) {
            $leftbaritem = array(
                'leftbar' => $leftbar->item
                );
        }

        $rightbar= Pagerightbar::find('pageid="' . $pageid . '"');
         foreach ($rightbar as $rightbar) {;
            $rightbaritem[] = array(
                'rightbar'=>$rightbar->item
                );
        }
        
        if ($pages) {
            $data = array(
                'pageid' => $pages->pageid,
                'title' => $pages->title,
                'metatitle' => $pages->metatitle,
                'metatags' => $pages->metatags,
                'metadesc' => $pages->metadesc,
                'pageslugs' => htmlentities($pages->pageslugs),
                'subtitle1' => $pages->subtitle1,
                'subtitle2' => $pages->subtitle2,
                'imagethumb' => $pages->imagethumb,
                'buttontitle' => $pages->buttontitle,
                'body' => $pages->body,
                'peoplesaying' => $pages->peoplesaying,
                'serviceprice' => $pages->serviceprice,
                'banner' => $pages->banner,
                'category' => $pages->category,
                'layout' => $pages->pagelayout,
                'leftbaritem' => $leftbaritem,
                'rightbaritem' => $rightbaritem,
                'imagethumbsubtitle' => $pages->imagethumbsubtitle,
                'thumbdesc' => $pages->thumbdesc

                );
        }
        echo json_encode($data);
    }




    public function saveeditedPagesAction() {

        $request = new \Phalcon\Http\Request();
        
        if($request->isPost()){

            $pageid = $request->getPost('pageid');
            $title = $request->getPost('title');
            $metatitle = $request->getPost('metatitle');
            $metatags = preg_replace('/^([^a-zA-Z0-9])*/', '', $request->getPost('metatags'));
            $metadesc = $request->getPost('metadesc');
            $slugs = $request->getPost('pageslugs');
            $subtitle1 = $request->getPost('subtitle1');
            $subtitle2 = $request->getPost('subtitle2');
            $imagethumb = $request->getPost('imagethumb');
            $buttontitle = $request->getPost('buttontitle');
            $body = $request->getPost('body');
            $peoplesaying = $request->getPost('peoplesaying');
            $serviceprice = $request->getPost('serviceprice');
            $banner = $request->getPost('banner');
            $category = $request->getPost('category');
            $leftbar = $request->getPost('leftbar');
            $rightbar = $request->getPost('rightbar');
            $imagethumbsubtitle = $request->getPost('imagethumbsubtitle');
            $thumbdesc  = $request->getPost('thumbdesc');


            $pages = Pages::findFirst('pageid="' . $pageid . '"');
            $pages->category = $category;
            $pages->title = $title;
            $pages->metatitle = $metatitle;
            $pages->metatags = $metatags;
            $pages->metadesc = $metadesc;
            $pages->pageslugs = $slugs;
            $pages->subtitle1 = $subtitle1;
            $pages->subtitle2 = $subtitle2;
            $pages->pageupdated_at = date("Y-m-d H:i:s");
            $pages->imagethumb = $imagethumb;
            $pages->buttontitle = $buttontitle;
            $pages->body = $body;
            $pages->peoplesaying = $peoplesaying;
            $pages->serviceprice = $serviceprice;
            $pages->banner = $banner;
            $pages->imagethumbsubtitle = $imagethumbsubtitle;
            $pages->thumbdesc = $thumbdesc;

            if (!$pages->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }
            echo json_encode($data);



        }
    }


    public function getPageAction($pageslugs) {
        
        $conditions = "pageslugs LIKE'" . $pageslugs . "'";
        echo json_encode(Pages::findFirst(array($conditions))->toArray(), JSON_NUMERIC_CHECK);

    }

    public function getPagecategoryAction($pageslugs){
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT pagecategory.title as categorytitle FROM pages INNER JOIN pagecategory ON pages.category = pagecategory.id WHERE pageslugs LIKE'" . $pageslugs . "'");

        $stmt->execute();
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        echo json_encode($data);
    }

    public function deletepagesimgAction()
    {
        $conditions = 'filename="' . $_POST['imgfilename'] . '"';
        $newsimg = Pageimage::findFirst(array($conditions));
        if ($newsimg) {
            if ($newsimg->delete()) {
                $data = array('success' => 'Image Deleted');
            }
            else
            {
                $data = array('error' => 'Image Not Deleted');
            }
        }
        echo json_encode($data);
    }

    public function createpagecategoryAction()
    {

        $guid = new \Utilities\Guid\Guid();
        $id = $guid->GUID();

         $request = new \Phalcon\Http\Request();
        
        if($request->isPost()){


            $title = $request->getPost('title');
            $subtitle = $request->getPost('subtitle');
            $colorlegend = $request->getPost('color');
            $metatitle = $request->getPost('metatitle');
            $metadesc = $request->getPost('metadesc');
            $metakeyword = $request->getPost('metakeyword');
            

            $guid = new \Utilities\Guid\Guid();
            $id = $guid->GUID();

            $page = new Pagecategory();
            $page->assign(array(
                'id' => $id,
                'title' => $title,
                'subtitle' => $subtitle,
                'date' =>date("Y-m-d H:i:s"),
                'colorlegend' => $colorlegend,
                'metatitle' => $metatitle,
                'metadesc' => $metadesc,
                'metakeyword' => $metakeyword
                ));

            if (!$page->save()) {
                $errors = array();
                foreach ($page->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {



                    $data['success'] = "Success";
                }


        }

        echo json_encode($data);
           
    }

    public function listpagecategoryAction() 
    {
        $pagecat = Pagecategory::find(array());
        $listcat = json_encode($pagecat->toArray(), JSON_NUMERIC_CHECK);
        echo $listcat;
    }    

    public function managePagesCategoryAction($num, $page, $keyword) {

        if ($keyword == 'null' || $keyword == 'undefined') {
            $Pages = Pagecategory::find();
        } else {
            $conditions = "title LIKE '%" . $keyword . "%'";
            $Pages = Pagecategory::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $Pages,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id' => $m->id,
                'title' => $m->title,
                'subtitle' => $m->subtitle,
                'colorlegend' => $m->colorlegend,
                'metatitle' => $m->metatitle,
                'metadesc' => $m->metadesc,
                'metakeyword' => $m->metakeyword
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }

    public function categorydeleteAction($catid) {
        $conditions = 'id="' . $catid . '"';
        $pages = Pagecategory::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($pages) {
            if ($pages->delete()) {
                $data = array('success' => 'Page Deleted');
            }
        }
        echo json_encode($data);
    }

    public function updatecategoryAction()
    {

        $data = array();
        $id = $_POST['id'];
        $pagecat = Pagecategory::findFirst('id="' . $id .'"');
        $pagecat->title = $_POST['title'];
        $pagecat->subtitle = $_POST['subtitle'];
        $pagecat->colorlegend = $_POST['color'];
        $pagecat->metatitle = $_POST['metatitle'];
        $pagecat->metadesc= $_POST['metadesc'];
        $pagecat->metakeyword= $_POST['metakeyword'];

        if (!$pagecat->save()) 
        {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } 
        else 
        {
            $data['success'] = "Success";
        }
        echo json_encode($data);
    }


    public function getAction(){
        $pages = Pages::find();
        echo json_encode($pages->toArray());
    }

    public function listmenuAction($cat) {
        
        $pagecat = Pagecategory::findFirst('title="' . $cat .'"');
        $catid = $pagecat->id;

        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT pages.title, pages.pageid, pages.thumbdesc, pages.subtitle1, pages.imagethumbsubtitle, pages.pageslugs,pages.buttontitle,pages.body, pages.peoplesaying, pages.status, pages.pagelayout, pages.type, pages.serviceprice, pages.banner, pages.category, pages.subtitle2, pages.imagethumb,  pagecategory.title as cattitle,  pagecategory.subtitle as catsubtitle FROM pages INNER JOIN pagecategory ON pagecategory.id = pages.category WHERE category LIKE '". $catid ."' AND status=1");

        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        echo json_encode($data);

    }

    public function addpriceAction(){

    }

    public function loadcatpagesAction($catid){
        $app = new CB();
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT pages.pageid, pages.title FROM pages LEFT JOIN pagecategory on pages.category = pagecategory.id WHERE pagecategory.id='$catid'");

        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $stmt = $db->prepare("SELECT * FROM pagecategory WHERE id != '$catid' ");

        $stmt->execute();
        $category = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        echo json_encode(array("dataconflict" => $data, "catpages" => $category));
    }

    public function updatepagescatAction(){
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $category = $request->getPost('catid');
            $pageid = $request->getPost('pageid');

            $pages = Pages::findFirst('pageid="' . $pageid . '"');
            $pages->category = $category;
            if(!$pages->save()){
                $errors = array();
                foreach ($pages->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            }
        }        
    }
    
}
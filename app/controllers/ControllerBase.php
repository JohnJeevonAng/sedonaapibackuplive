<?php

namespace Controllers;

class ControllerBase extends \Phalcon\Mvc\Controller {

    public function modelsManager($phql) {
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        return $result = $app->modelsManager->executeQuery($phql);
    }

    public function dbSelect($phql) {
        $db1 = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db1->prepare($phql);
        $stmt->execute();
        $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $searchresult;
    }

    public function getConfig(){
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        return $app->config;
    }

    public function sendMail($email, $subject,$content){
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        $json = json_encode(array(
            'From' => $app->config->postmark->signature,
            'To' => $email,
            'Subject' => $subject,
            'HtmlBody' => $content
        ));

        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL, $app->config->postmark->url);
        curl_setopt($ch2, CURLOPT_POST, true);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'X-Postmark-Server-Token: '.$app->config->postmark->token
        ));
        curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
        $response = json_decode(curl_exec($ch2), true);
        $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
        curl_close($ch2);
        return $http_code;
    }

    public function mailTemplate($heading, $body){
        $content = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html lang=en> <head><meta http-equiv=Content-Type content="text/html; charset=UTF-8"><meta name=viewport content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in --><meta http-equiv=X-UA-Compatible content=IE=edge><!-- enable media queries for windows phone 8 -->
        <meta name=format-detection content=telephone=no><!-- disable auto telephone linking in iOS --><title>Sedona Healing Arts</title><style type=text/css>
        body{margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;}table{border-spacing: 0;}table td{border-collapse: collapse;}
        .ExternalClass{width: 100%;}.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height: 100%;}
        .ReadMsgBody {width: 100%;background-color: #ebebeb;}table {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}img {-ms-interpolation-mode: bicubic;}
        .yshortcuts a {border-bottom: none !important;}@media screen and (max-width: 599px) {.force-row,.container {width: 100% !important;max-width: 100% !important;}}
        @media screen and (max-width: 400px) {.container-padding {padding-left: 12px !important;padding-right: 12px !important;}}.ios-footer a {color: #aaaaaa !important;
        text-decoration: underline;}</style></head> <body style="margin:0; padding:0;" bgcolor=#F0F0F0 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0> 
        <!-- 100% background wrapper (grey background) --> <table border=0 width=100% height=100% cellpadding=0 cellspacing=0 bgcolor=#F0F0F0> <tr> 
        <td align=center valign=top bgcolor=#F0F0F0 style="background-color: #F0F0F0;"> <br> <!-- 600px container (white background) --> 
        <table border=0 width=600 cellpadding=0 cellspacing=0 class=container style=width:600px;max-width:600px> <tr> 
        <td class="container-padding header" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#DF4726;padding-left:24px;padding-right:24px"> Sedona Healing Arts </td> 
        </tr> <tr> <td class="container-padding content" align=left style=padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff> <br> 
        <div class=title style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550">'.$header.'</div> <br> 
        <div class=body-text style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">'.$body.'<br><br></div> 
        </td> </tr> <tr> <td class="container-padding footer-text" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px"> 
        <br><br> Copyright: © Sedona Healing Arts <br><br> <strong>Sedona Healing Arts</strong><br> <span class=ios-footer> 201 State Route 179,<br> Sedona, AZ 86336<br> </span> 
        <a href="http://www.sedonahealingarts.com" style=color:#aaaaaa>www.sedonahealingarts.com</a><br> <br><br> </td> </tr> </table> <!--/600px container --> </td> </tr> </table> <!--/100% background wrapper--> </body> </html>'; 
        return $content;
    }

    public function creditcardPayment($cred, $invoiceno ,$description){
        $DI = \Phalcon\DI::getDefault();
        $dc = $DI->get('application');
        $loginname = '';
        $transactionkey = '';
        $donatorfName = $cred['billinginfo']['billingfname'];
        $donatorlName = $cred['billinginfo']['billinglname'];
        $host = '';
        $path = '';
        if($dc->config->development){
            $post_url = $dc->config->authorizedtest->post_url;
            $loginname=$dc->config->authorizedtest->apilogin;
            $transactionkey=$dc->config->authorizedtest->transactionkey;
            $host = $dc->config->authorizedtest->apiurlHost;
            $path = $dc->config->authorizedtest->apiurlVer;
        }else{
            $post_url = $dc->config->authorized->post_url;
            $loginname=$dc->config->authorized->apilogin;
            $transactionkey=$dc->config->authorized->transactionkey;
            $host = $dc->config->authorized->apiurlHost;
            $path = $dc->config->authorized->apiurlVer;
        }

        $amount = 0;

        foreach($cred['reservationlist'] as $val){
            $amount += $val['servicechoice']['price'];
        }

        $post_values='';

        if($cred['paymenttype']=='card'){

            $post_values = array(

                // the API Login ID and Transaction Key must be replaced with valid values
                "x_login"			=> $loginname,
                "x_tran_key"		=> $transactionkey,
                "x_first_name" => $donatorfName,
                "x_last_name" => $donatorlName,

                "x_version"			=> "3.1",
                "x_delim_data"		=> "TRUE",
                "x_delim_char"		=> "|",
                "x_relay_response"	=> "FALSE",
                "x_invoice_num"     => $invoiceno,
                "x_type"			=> "AUTH_CAPTURE",
                "x_method"			=> "CC",
                "x_card_num"		=> $cred['billinginfo']['ccn'],
                "x_exp_date"		=> sprintf("%02d", $cred['billinginfo']['expiremonth']).substr( $cred['billinginfo']['expireyear'], -2 ),
                "x_amount"			=> $amount,
                "x_description"		=> $donatorfName . ' ' . $donatorlName .' - '.$description,

                // Additional fields can be added here as outlined in the AIM integration
                // guide at: http://developer.authorize.net
            );

            $post_values["x_address"] = $cred['billinginfo']['al1'] . $cred['billinginfo']['al2'];
            $post_values["x_city"] = $cred['billinginfo']['city'];
            $post_values["x_state"] = $cred['billinginfo']['state'];
            $post_values["x_zip"] = $cred['billinginfo']['zip'];
            $post_values["x_country"] = $cred['billinginfo']['country']['name'];
        }else{
            $post_values = array(

                // the API Login ID and Transaction Key must be replaced with valid values
                "x_login"           => $loginname,
                "x_tran_key"        => $transactionkey,
                "x_first_name" => $donatorfName,
                "x_last_name" => $donatorlName,

                "x_version"         => "3.1",
                "x_delim_data"      => "TRUE",
                "x_delim_char"      => "|",
                "x_relay_response"  => "FALSE",
                "x_invoice_num"     => $invoiceno,

                "x_method"          => "ECHECK",
                "x_bank_aba_code"   => $cred['billinginfo']['bankrouting'],
                "x_bank_acct_num"   => $cred['billinginfo']['bankaccountnumber'],
                "x_bank_acct_type"  => $cred['billinginfo']['at'],
                "x_bank_name"       => $cred['billinginfo']['bankname'],
                "x_bank_acct_name"  => $cred['billinginfo']['accountname'],


                "x_amount"          => $amount,
                "x_description"     => $donatorfName . ' ' . $donatorlName .' - '.$description,

                // Additional fields can be added here as outlined in the AIM integration
                // guide at: http://developer.authorize.net
            );

            if($cred['billinginfo']['at'] == 'BUSINESSCHECKING'){
                $post_values["x_echeck_type"] = 'CCD';
            }else{
                $post_values["x_echeck_type"] = 'WEB';
            }

            $post_values["x_address"] = $cred['billinginfo']['al1'] . $cred['billinginfo']['al2'];
            $post_values["x_city"] = $cred['billinginfo']['city'];
            $post_values["x_state"] = $cred['billinginfo']['state'];
            $post_values["x_zip"] = $cred['billinginfo']['zip'];
            $post_values["x_country"] = $cred['billinginfo']['country']['name'];

        }


        // This section takes the input fields and converts them to the proper format
        // for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
        $post_string = "";
        foreach( $post_values as $key => $value )
        { $post_string .= "$key=" . urlencode( $value ) . "&"; }
        $post_string = rtrim( $post_string, "& " );

        // The following section provides an example of how to add line item details to
        // the post string.  Because line items may consist of multiple values with the
        // same key/name, they cannot be simply added into the above array.
        //
        // This section is commented out by default.

//        $line_items = $items;
//
//        foreach( $line_items as $value )
//            { $post_string .= "&x_line_item=" . urlencode( $value ); }
//

        // This sample code uses the CURL library for php to establish a connection,
        // submit the post, and record the response.
        // If you receive an error, you may want to ensure that you have the curl
        // library enabled in your php configuration
        $request = curl_init($post_url); // initiate curl object
        curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
        $post_response = curl_exec($request); // execute curl post and store results in $post_response
        // additional options may be required depending upon your server configuration
        // you can find documentation on curl options at http://www.php.net/curl_setopt
        curl_close ($request); // close curl object

        // This line takes the response and breaks it into an array using the specified delimiting character
        $response_array = explode($post_values["x_delim_char"],$post_response);

        // The results are output to the screen in the form of an html numbered list.
        if($response_array[0]==1){
            return array('success'=>'Success.', 'code' => $response_array);
        }else{
            return array(
                'error'=>'Something went wrong in processing your transaction. Please check you Credit Card or verify your provider.',
                'code' => $response_array
            );
        }
    }

}

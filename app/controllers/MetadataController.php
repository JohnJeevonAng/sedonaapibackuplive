<?php

namespace Controllers;


use \Models\Metadata as Metadata;
use \Models\Pagecategory as Pagecategory;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use PHPMailer as PHPMailer;

class MetadataController extends \Phalcon\Mvc\Controller {

    public function listAction($num, $page, $keyword) {
        $app = new CB();

        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT * FROM metadata';
        $sqlCount = 'SELECT COUNT(*) FROM metadata';

        if ($keyword != 'null' && $keyword != 'undefined') {
            $sqlWhere = " WHERE module LIKE '%" . $keyword . "%' OR metatitle LIKE '%" . $keyword . "' OR metakeyword LIKE '%".$keyword."%' ";
            $sql .= $sqlWhere;
            $sqlCount .= $sqlWhere;
        }

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }
        // $sql .= " ORDER BY updated_at DESC ";
        $sql .= " LIMIT " . $offsetfinal . ",10";
        // getting the query
        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);

        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));
    
    }


     public function editAction() {

        $request = new \Phalcon\Http\Request();
        
        if($request->isPost()){
            $data = array();
            $id = $request->getPost('id');
            $metatitle = $request->getPost('metatitle');
            $metadesc = $request->getPost('metadesc');
            $metakeyword = $request->getPost('metakeyword');

            $page = Metadata::findFirst('id="' . $id . '"');
            $page->metatitle = $metatitle;
            $page->metadesc = $metadesc;
            $page->metakeyword = $metakeyword;
            if (!$page->save()) {
                $data['error'] = "Something went wrong saving page status, please try again.";
            } else {
                $data['success'] = "Success";
            }

            echo json_encode($data);
        }

    }

     public function showAction($data) {
        $app = new CB();
        $sql = "SELECT * from metadata WHERE module='".$data."'";
        $searchresult = $app->dbSelect($sql);
        echo json_encode($searchresult);
    }

    public function showcatAction($data) {
        $app = new CB();
        $sql = "SELECT * from pagecategory WHERE title='".$data."'";
        $searchresult = $app->dbSelect($sql);
        echo json_encode($searchresult);
    }
}


<?php

namespace Controllers;


use \Controllers\ControllerBase as CB;
use \Models\Testimonials as Testimonials;
use \Models\Pages as Pages;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
class TestimonialsController extends \Controllers\ControllerBase {
    public function listAction($num, $page, $keyword) {

        $app = new CB();

        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT * FROM testimonials INNER JOIN pages ON testimonials.pageid = pages.pageid';
        $sqlCount = 'SELECT COUNT(*) FROM testimonials INNER JOIN pages ON testimonials.pageid = pages.pageid';

        if ($keyword != 'null' && $keyword != 'undefined') {
            $sqlQuery = " WHERE name LIKE '%" . $keyword . "%' OR message LIKE '%" . $keyword . "' OR title LIKE '%".$keyword."%' ";
                $sql .=  $sqlQuery;
            $sqlCount .= $sqlQuery;
        }

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }
        $sql .= " ORDER BY updated_at DESC ";
        $sql .= " LIMIT " . $offsetfinal . ", ".$num;
        // getting the query
        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);

        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));

    }
    public function createAction(){
        $data = array();
        $guid = new \Utilities\Guid\Guid();

        $testi = new Testimonials();
        if($_POST['files'] == null){
            $pic = "man-303792_640.png";
        }else {
            $pic = $_POST['files'];
        }
        $testi->assign(array(
            'id' => $guid->GUID(),
            'name' => $_POST['name'],
            'message' => $_POST['message'],
            'pageid' => $_POST['page'],
            'picture' => $pic,
            'date' => $_POST['dt'],
            'created_at' =>  date("Y-m-d H:i:s"),
            'updated_at' =>  date("Y-m-d H:i:s"),
        ));
        if (!$testi->save()){
            $errors = array();
            foreach ($testi->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            echo json_encode(array('error' => $errors));
        }
        else{
            $data['success'] = "Success";
        }
        echo json_encode($data);
    }
    public function getAction($id){
        $testi = Testimonials::find('id="'.$id.'"');
        echo json_encode($testi->toArray());
    }
    public function updateAction(){
        $testi = Testimonials::findFirst('id="'.$_POST['id'].'"');
        if ($testi) {

                $testi->name = $_POST['name'];
                $testi->message = $_POST['message'];
                $testi->pageid = $_POST['page'];
            if(!empty($_POST['files'])){
                $testi->picture = $_POST['files'];
            }
                $testi->date = $_POST['dt'];

            if (!$testi->save()) {
                $data['error'] = 'Something went wrong.';
            } else {

                $data['success'] = "Success";
            }
        }else{
            $data['error'] = 'Testi not found';
        }
        echo json_encode($data);
    }
    public function deleteAction($id){
        $conditions = 'id="' . $id . '"';
        $testi = Testimonials::findFirst(array($conditions));
        if ($testi) {
            if ($testi->delete()) {
                $data = array('success' => 'Testimonial Deleted');
            }
            else
            {
                $data = array('error' => 'Testimonial Not Deleted');
            }
        }
        echo json_encode($data);

    }


    public function gettestimonialAction($pageslugs) {

        $conditions = 'pageslugs="' . $pageslugs . '"';
        $getpageID = Pages::findFirst(array($conditions));
        $pageID = $getpageID->pageid;

        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM testimonials  WHERE pageid = '". $pageID ."' ORDER BY updated_at DESC");

        $stmt->execute();
        $testimonials = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        echo json_encode($testimonials);

    }
}

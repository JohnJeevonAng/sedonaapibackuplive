<?php

namespace Controllers;

use \Models\Contactus as Contactus;
use \Models\Contactusreply as Contactusreply;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use PHPMailer as PHPMailer;

class ContactusController extends \Phalcon\Mvc\Controller {

    public function sendAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $guid = new \Utilities\Guid\Guid();
            $id = $guid->GUID();
            $subject = $request->getPost('subject');
            $email = $request->getPost('email');
            $name = ucwords(strtolower($request->getPost('fname')." ".$request->getPost('lname')));
            $message = $request->getPost('message');
            $date = date('Y-m-d H:i:s');

            $contactus = new Contactus();
            $contactus->assign(array(
              'id' => $id,
              'subject' => $subject,
              'name' => $name,
              'email' => $email,
              'message' => $message,
              'date_created' => $date,
              'updated_at' => $date,
              'status' => 'new'
             ));

             if($contactus->save()){
                // echo "Feedback has been sent";
                $app = new CB();
                $adminemail = 'contact@sedonahealingarts.com';
                // $adminemail = 'sedonahealingarts@mailinator.com';
                $heading = $subject;
                $b = "Name:".$name."<br> Email: ".$email." <br> Message: <br>". $message;
                $con = $app->mailTemplate($heading, $b);
                $sendtoadmin = new PHPMailer();

                $sendtoadmin->isSMTP();
                $sendtoadmin->Host = 'smtp.mandrillapp.com';
                $sendtoadmin->SMTPAuth = true;
                $sendtoadmin->Username = 'efrenbautistajr@gmail.com';
                $sendtoadmin->Password = '6Xy18AJ15My59aQNTHE5kA';
                        //$mail->SMTPSecure = 'ssl';
                $sendtoadmin->Port = 587;
                $sendtoadmin->From = $email;
                $sendtoadmin->FromName = $name;
                $sendtoadmin->addAddress($adminemail);
                $sendtoadmin->isHTML(true);
                $sendtoadmin->Subject = $heading;
                $sendtoadmin->Body = $con;

                $sub = "Hi ".$name;
                $body = "Thank you for contacting Sedona Healing Arts, your message was sent successfully.<br>We will review your inquiry and get back to you shortly.";
                $content = $app->mailTemplate($sub, $body);

                $mail = new PHPMailer();
                $mail->isSMTP();
                $mail->Host = 'smtp.mandrillapp.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'efrenbautistajr@gmail.com';
                $mail->Password = '6Xy18AJ15My59aQNTHE5kA';
                        //$mail->SMTPSecure = 'ssl';                    
                $mail->Port = 587;
                $mail->From = $adminemail;
                $mail->FromName = 'Sedona Healing Arts Team';
                $mail->addAddress($email);
                $mail->isHTML(true);
                $mail->Subject = 'Re -'.$subject;
                $mail->Body = $content;

                if (!$sendtoadmin->send()){
                    $data = array('error' => $sendtoadmin->ErrorInfo);
                    die(json_encode(array("error" =>  $sendtoadmin->ErrorInfo)));
                }
                elseif (!$mail->send()){
                    $data = array('error' => $mail->ErrorInfo);
                    die(json_encode(array("error" =>  $mail->ErrorInfo)));
                }

                // $heading = "Hi ".$name;
                // $body = "Thank you for contacting Sedona Healing Arts, your message was sent successfully.<br>We will review your inquiry and get back to you shortly.";
                // $content = $app->mailTemplate($heading, $body);
                // $app->sendMail($email, 'Re -'.$subject, $content);

                // $email = 'efrensedona@mailinator.com';
                // $heading = "Name: ".$name . " <br> Email: ". $senderemail;
                // $body = "Message: <br>". $message;
                // $content = $app->mailTemplate($heading, $body);
                // $app->sendMail($email,$subject,$content);
             } else {
               $errors = array();
               foreach ($contactus->getMessages() as $message) {
                   $errors[] = $message->getMessage();
               }
               echo json_encode(array('error' => $errors, 'msg' => 'Something went wrong, please try again later.'));
             }
        }
    }

    public function listAction($num, $page, $keyword, $searchdate){
        $app = new CB();

        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT * FROM contactus';
        $sqlCount = 'SELECT COUNT(*) FROM contactus';

        if ($keyword != 'null' && $keyword != 'undefined') {
            $sqlWhere = " WHERE name LIKE '%" . $keyword . "%' OR subject LIKE '%" . $keyword . "' OR email LIKE '%".$keyword."%' OR message LIKE '%".$keyword."%' OR DATE_FORMAT(date_created,'%Y-%m-%d' ) = '".$searchdate."'";
            $sql .= $sqlWhere;
            $sqlCount .= $sqlWhere;
        }elseif(($keyword == 'null' || $keyword == 'undefined')  && $searchdate != 'null' && $searchdate != 'undefined'){
            $sqlWhere = " WHERE DATE_FORMAT(date_created,'%Y-%m-%d' ) = '".$searchdate."'";
            $sql .= $sqlWhere;
            $sqlCount .= $sqlWhere;
        }

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }
        $sql .= " ORDER BY updated_at DESC ";
        $sql .= " LIMIT " . $offsetfinal . ",10";
        // getting the query
        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);

        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));
    }

    public function deleteAction($id){
        $msg = Contactus::findFirst('id="' . $id . '"');
        if($msg){
          if($msg->delete()){
            $data = array('success' => 'Message Deleted');
            die(json_encode($data));
          }else {
              $data = array('error' => 'Message Not Deleted');
              die(json_encode($data));
          }
        }
    }

    public function getAction($id) {
            $msg = Contactus::find('id="'.$id.'"');
            echo json_encode($msg->toArray());
    }

    public function readAction($id){
        $msg = Contactus::findFirst('id="'.$id.'"');
        $msg->status='read';
        if (!$msg->save()) {
            $data['error'] = 'Something went wrong.';
        } else {

            $data['success'] = "Success";
        }
        echo json_encode($data);
    }

    public function getrepliesAction($id){
        $msg = Contactusreply::find(array(
            "contactusid = '".$id."'",
            "order" => "updated_at ASC",

        ));
        echo json_encode($msg->toArray(), JSON_NUMERIC_CHECK);
    }

    public function replyAction(){
        $data = array();
        $guid = new \Utilities\Guid\Guid();
        $app = new CB();

        $msg = new Contactusreply();
        $msg->assign(array(
            'id' => $guid->GUID(),
            'message' => $_POST['message'],
            'contactusid' => $_POST['contactusid'],
            'created_at' =>  date("Y-m-d H:i:s"),
            'updated_at' =>  date("Y-m-d H:i:s"),
        ));
        if (!$msg->save()){
            $errors = array();
            foreach ($msg->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            echo json_encode(array('error' => $errors));
        }
        else{
            $data['success'] = "Success";
            $rcontactus = Contactus::findFirst('id="'.$_POST['contactusid'].'"');
            $rcontactus->status='replied';
            if (!$rcontactus->save()) {
                $data['error'] = 'Something went wrong.';
            } else {
                $heading = $rcontactus->subject;
                $body = "Hi ".$rcontactus->name.",<br><br>".$_POST['message']."<br><br>====================================================<br><br>";
                $body .= "Your Message:";
                $body .= "<br><br>".$rcontactus->message;
                $content = $app->mailTemplate($heading, $body);
                $app->sendMail($rcontactus->email, 'Re -'.$rcontactus->subject, $content);

                $data['success'] = "Success";
            }
        }
        echo json_encode($data);
    }
}

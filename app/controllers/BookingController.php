<?php

namespace Controllers;


use \Models\Booking as Booking;
use \Models\Bookingreply as Bookingreply;
use \Models\Reservations as Reservations;
use \Models\Schedules as Schedules;
use \Models\Members as Members;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use PHPMailer as PHPMailer;

class BookingController extends \Phalcon\Mvc\Controller {

    public function sendbookingAction() {
         // var_dump($_POST);

        $request = new \Phalcon\Http\Request();

        if($request->isPost()){


            $lname = $request->getPost('lname');
            $fname = $request->getPost('fname');
            $senderemail = $request->getPost('email');
            $contactno = $request->getPost('contactno');
            $service = $request->getPost('service');
            $message = $request->getPost('message');

            $guid = new \Utilities\Guid\Guid();
            $id = $guid->GUID();

            $book = new Booking();
            $book->assign(array(
                'id' => $id,
                'lname' => $lname,
                'fname' => $fname,
                'email' => $senderemail,
                'contactno' => $contactno,
                'service' => $service,
                'message' => $message,
                'status' => 'new',
                'created_at' =>  date("Y-m-d H:i:s"),
                'updated_at' =>  date("Y-m-d H:i:s")
                ));

            if (!$book->save()) {
                $errors = array();
                foreach ($book->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                $data['success'] = "Success";

                $app = new CB();

                $adminemail = 'contact@sedonahealingarts.com';
                // $adminemail = 'sedonahealingarts@mailinator.com';
                $subject = $service;
                $body = 'Name: '.$fname. ' '. $lname . ' <br> Email: '. $senderemail . ' <br> Contact Number: '. $contactno . ' <br><br> Message: <br><br>'. $message . '';
                $content = $app->mailTemplate($subject, $body);
                /*$app->sendMail($email,$subject,$content);

                $subject = "CC:".$service;
                $body = 'Name: '.$fname. ' '. $lname . ' <br> Email: '. $senderemail . ' <br> Contact Number: '. $contactno . ' <br><br> Message: <br><br>'. $message . '';
                $content = $app->mailTemplate($subject, $body);
                $app->sendMail($senderemail,$subject,$content);*/


                $sendtoadmin = new PHPMailer();

                $sendtoadmin->isSMTP();
                $sendtoadmin->Host = 'smtp.mandrillapp.com';
                $sendtoadmin->SMTPAuth = true;
                $sendtoadmin->Username = 'efrenbautistajr@gmail.com';
                $sendtoadmin->Password = '6Xy18AJ15My59aQNTHE5kA';
                        //$mail->SMTPSecure = 'ssl';
                $sendtoadmin->Port = 587;
                $sendtoadmin->From = $senderemail;
                $sendtoadmin->FromName = 'Sedona Healing Arts Team';
                $sendtoadmin->addAddress($adminemail);
                $sendtoadmin->isHTML(true);
                $sendtoadmin->Subject = $subject;
                $sendtoadmin->Body = $content;


                $mail = new PHPMailer();

                $mail->isSMTP();
                $mail->Host = 'smtp.mandrillapp.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'efrenbautistajr@gmail.com';
                $mail->Password = '6Xy18AJ15My59aQNTHE5kA';
                        //$mail->SMTPSecure = 'ssl';                    
                $mail->Port = 587;
                $mail->From = $adminemail;
                $mail->FromName = 'Sedona Healing Arts Team';
                $mail->addAddress($senderemail);
                $mail->isHTML(true);
                $mail->Subject = 'Re -'.$subject;
                $mail->Body = $content;

                if (!$sendtoadmin->send()){
                    $data = array('error' => $sendtoadmin->ErrorInfo);
                    die(json_encode(array("error" =>  $sendtoadmin->ErrorInfo)));
                }
                elseif (!$mail->send()){
                    $data = array('error' => $mail->ErrorInfo);
                    die(json_encode(array("error" =>  $mail->ErrorInfo)));
                }
            }

        }

        echo json_encode($data);



    }

    public function listAction($num, $page, $keyword, $searchdate) {
        $app = new CB();

        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT * FROM booking';
        $sqlCount = 'SELECT COUNT(*) FROM booking';

        if ($keyword != 'null' && $keyword != 'undefined') {
            $sqlWhere = " WHERE fname LIKE '%" . $keyword . "%' OR lname LIKE '%" . $keyword . "' OR email LIKE '%".$keyword."%' OR service LIKE '%".$keyword."%' OR message LIKE '%".$keyword."%' OR DATE_FORMAT(created_at,'%Y-%m-%d' ) = '".$searchdate."'";
            $sql .= $sqlWhere;
            $sqlCount .= $sqlWhere;
        }elseif(($keyword == 'null' || $keyword == 'undefined')  && $searchdate != 'null' && $searchdate != 'undefined'){
            $sqlWhere = " WHERE DATE_FORMAT(created_at,'%Y-%m-%d' ) = '".$searchdate."'";
            $sql .= $sqlWhere;
            $sqlCount .= $sqlWhere;
        }

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }
        $sql .= " ORDER BY updated_at DESC ";
        $sql .= " LIMIT " . $offsetfinal . ",10";
        // getting the query
        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);

        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));
    }

    public function getAction($id) {
            $book = Booking::find('id="'.$id.'"');
            echo json_encode($book->toArray());
    }
    public function readAction($id){
        $book = Booking::findFirst('id="'.$id.'"');
        $book->status='read';
        if (!$book->save()) {
            $data['error'] = 'Something went wrong.';
        } else {

            $data['success'] = "Success";
        }
        echo json_encode($data);
    }
    public function replyAction(){
        $data = array();
        $guid = new \Utilities\Guid\Guid();

        $book = new Bookingreply();
        $book->assign(array(
            'id' => $guid->GUID(),
            'message' => $_POST['message'],
            'bookid' => $_POST['bookid'],
            'created_at' =>  date("Y-m-d H:i:s"),
            'updated_at' =>  date("Y-m-d H:i:s"),
        ));
        if (!$book->save()){
            $errors = array();
            foreach ($book->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            echo json_encode(array('error' => $errors));
        }
        else{
            $data['success'] = "Success";
            $rbook = Booking::findFirst('id="'.$_POST['bookid'].'"');
            $rbook->status='replied';
            if (!$rbook->save()) {
                $data['error'] = 'Something went wrong.';
            } else {
                
                $app = new CB();
                $body = $_POST['message']."<br/><br/> Message ".$rbook->updated_at." ===================================<br/><br/>".$rbook->message;
                $content = $app->mailTemplate($rbook->service, $body);
                //$app->sendMail($rbook->email, 'Re -'.$rbook->service, $content);

                $adminemail = 'contact@sedonahealingarts.com';
                // $adminemail = 'geeksnest@mailinator.com';
                $mail = new PHPMailer();

                $mail->isSMTP();
                $mail->Host = 'smtp.mandrillapp.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'efrenbautistajr@gmail.com';
                $mail->Password = '6Xy18AJ15My59aQNTHE5kA';
                        //$mail->SMTPSecure = 'ssl';                    
                $mail->Port = 587;
                $mail->From = $adminemail;
                $mail->FromName = 'Sedona Healing Arts Team';
                $mail->addAddress($rbook->email);
                $mail->isHTML(true);
                $mail->Subject = 'Re -'.$rbook->service;
                $mail->Body = $content;

                if (!$mail->send()){
                    $data = array('error' => $mail->ErrorInfo);
                    die(json_encode(array("error" =>  $mail->ErrorInfo)));
                }
                else{
                     $data['success'] = "Success";
                }
            }
        }
        echo json_encode($data);
    }
    public function getrepliesAction($id) {
        $book = Bookingreply::find(array(
            "bookid = '".$id."'",
            "order" => "updated_at ASC",

        ));
        echo json_encode($book->toArray());
    }
    public function deleteAction($id){
        $conditions = 'id="' . $id . '"';
        $book = Booking::findFirst(array($conditions));
        if ($book) {
            if ($book->delete()) {
                $data = array('success' => 'Booking Deleted');
                $rbook = Bookingreply::findFirst('bookid="' . $id . '"');
                if ($rbook) {
                    if ($rbook->delete()) {
                        $data = array('success' => 'Booking Deleted');
                    }
                    else
                    {
                        $data = array('error' => 'Booking Not Deleted');
                    }
                }
                die(json_encode($data));
            }
            else
            {
                $data = array('error' => 'Booking Not Deleted');
                die(json_encode($data));
            }
        }
    }
    public function addreservationAction(){
        $request = new \Phalcon\Http\Request();
        $cb = new CB();
        $response = '';
        $invoiceno = hexdec(substr(uniqid(),0,9));

        if($request->getPost('paymenttype')!='cash')
        {
            $data = $cb->creditcardPayment($_POST, $invoiceno , 'Offline Reservation');
            $response['authorize'] = $data;
        }

        if(isset($data['success']) || $request->getPost('paymenttype')=='cash'){

            if ($request->isPost() == true) {
                try{


                    $transactionManager = new TransactionManager();
                    $guid = new \Utilities\Guid\Guid();

                    $transaction = $transactionManager->get();

                    $members = Members::findFirst('email="'.$request->getPost('email').'"');
                    if (!$members) {
                        $members = new Members();
                        $members->setTransaction($transaction);

                        $members->memberid = $guid->GUID();
                        $members->email = $request->getPost('email');
                        $members->address1 = $request->getPost('address1');
                        $members->address2 = $request->getPost('address2');
                        $members->country = $request->getPost('country');
                        $members->firstname = $request->getPost('firstname');
                        $members->lastname = $request->getPost('lastname');
                        $members->phoneno = $request->getPost('phoneno');
                        $members->state = $request->getPost('state');
                        $members->zipcode = $request->getPost('zipcode');
                        $members->status = 1;
                        $members->created_at = date("Y-m-d H:i:s");
                        $members->updated_at = date("Y-m-d H:i:s");
                        if ($members->save() == false) {
                            $array = [];
                            foreach ($members->getMessages() as $message) {
                                $array[] = $message;
                            }
                            var_dump($array);
                            die();
                            $transaction->rollback('Cannot save members.');
                        }
                    }

                    $b = $request->getPost('billinginfo');

                    $res = new Reservations();
                    $res->setTransaction($transaction);
                    $res->id = $guid->GUID();
                    $res->invoiceno = $invoiceno;
                    $res->memberid = $members->memberid;
                    $res->billingfname = $b['billingfname'];
                    $res->billinglname = $b['billinglname'];
                    $res->billingcity = $b['city'];
                    $res->billingcountry = $b['country']['name'];
                    $res->paymenttype = $request->getPost('paymenttype');
                    $res->billingstate = $b['state'];
                    $res->billingzipcode = $b['zip'];
                    $res->billingadd1 = $b['al1'];
                    $res->billingadd2 = $b['al2'];
                    $res->amount = $request->getPost('amount');
                    $res->created_at = date("Y-m-d H:i:s");
                    $res->updated_at = date("Y-m-d H:i:s");
                    if($res->save()==false){
                        $array = [];
                        foreach ($res->getMessages() as $message) {
                            $array[] = $message;
                        }
                        $transaction->rollback('Cannot save Invoice.');
                    }

                    $ss = $request->getPost('reservationlist');
                    foreach($ss as $m => $k) {
                        $sched = new Schedules();
                        $sched->setTransaction($transaction);
                        $sched->id = $guid->GUID();
                        $sched->memberid = $members->memberid;
                        $sched->scheduledate = $k['reservationdate'];
                        $sched->startdatetime = $k['startdatetime'];
                        $sched->enddatetime = $k['enddatetime'];
                        $sched->serviceid = $k['servicechoice']['serviceid'];
                        $sched->invoiceid = $invoiceno;
                        $sched->price = $k['servicechoice']['price'];
                        $sched->description = $k['servicechoice']['description'];
                        $sched->hourday = $k['servicechoice']['hourday'];
                        $sched->status = 'RESERVED';
                        $sched->created_at = date("Y-m-d H:i:s");
                        $sched->updated_at = date("Y-m-d H:i:s");
                        if ($sched->save() == false) {
                            $array = [];
                            foreach ($sched->getMessages() as $message) {
                                $array[] = $message;
                            }
                            $transaction->rollback('Cannot save reservation.');
                        }
                    }

                    $transaction->commit();
                }catch (\Phalcon\Mvc\Model\Transaction\Failed $e) {
                    die( json_encode(array('401' => $e->getMessage())) );
                }

                $response['200'] = "Success.";
                $response['invoiceno'] = $invoiceno;
                die(json_encode($response));
            }
        }else{
            die(json_encode($data));
        }

    }

    public function validateAction($stime, $etime){
        $book = Schedules::find("'$stime' < enddatetime AND '$etime' > startdatetime AND status != 'VOID'");
        echo json_encode($book->toArray());
    }

    public function getbyemailAction($email){
        $member = Members::find("email='$email'");
        echo json_encode($member->toArray());
    }

    public function getinvoiceAction($invoiceno){
        $app = new CB();

        // getting the query
        $sql = 'SELECT * FROM reservations INNER JOIN schedules ON invoiceid = invoiceno INNER JOIN members ON reservations.memberid = members.memberid INNER JOIN pages ON pages.pageid=schedules.serviceid WHERE invoiceno = "'.$invoiceno.'"';
        $get = $app->dbSelect($sql);
        die(json_encode($get));
    }


    public  function getmontheventsAction($month,$year){
        $app = new CB();

        // getting the query
        $sql = 'SELECT schedules.id as schedid, schedules.startdatetime,  schedules.enddatetime,  schedules.scheduledate, schedules.status, members.firstname, members.lastname, schedules.hourday, schedules.description, reservations.invoiceno,  pages.title, pagecategory.colorlegend FROM reservations INNER JOIN schedules ON invoiceid = invoiceno INNER JOIN members ON reservations.memberid = members.memberid INNER JOIN pages ON pages.pageid=schedules.serviceid INNER JOIN pagecategory ON pages.category = pagecategory.id WHERE YEAR(schedules.scheduledate) = '.$year.' AND MONTH(schedules.scheduledate)='.$month;
        $get = $app->dbSelect($sql);
        die(json_encode($get));
    }

    public  function updatesheduleAction(){
        $request = new \Phalcon\Http\Request();
        $book = Schedules::find("('".$request->getPost('cstime')."' < enddatetime AND '".$request->getPost('cetime')."' > startdatetime) AND id != '".$request->getPost('schedid')."' AND status != 'VOID'");
        if(count($book->toArray()) > 0){
            die(json_encode(array("error"=>"Someone is already reserved in that schedule.")));
        }else{
            $data = array();
            $b = Schedules::findFirst("id = '".$request->getPost('schedid')."'");
            $b->scheduledate = $request->getPost('reservation');
            $b->startdatetime = $request->getPost('cstime');
            $b->enddatetime = $request->getPost('cetime');
            $b->updated_at = date("Y-m-d H:i:s");
            if (!$b->save()) {
                $data['error'] = "Something went wrong saving page status, please try again.";
            } else {
                $data['success'] = "Success";
            }
            die(json_encode($data));
        }
    }

    public function updatestatusAction($id, $status){
        $b = Schedules::findFirst("id = '".$id."'");
        $b->status = $status;
        $b->updated_at = date("Y-m-d H:i:s");
        if (!$b->save()) {
            $data['error'] = "Something went wrong saving page status, please try again.";
        } else {
            $data['success'] = "Success";
        }
        die(json_encode($data));
    }

    public function countingstatusAction($currtime){
        $app = new CB();
        // getting the query
        $sql = "
        SELECT
            SUM(status = 'DONE') AS done,
            SUM(status = 'VOID') AS void,
            SUM(status = 'RESERVED' && startdatetime >= '$currtime' && enddatetime >= '$currtime') AS reserved,
            SUM(status = 'RESERVED' && startdatetime <= '$currtime' && enddatetime <= '$currtime') AS unattended
        FROM schedules
        ";
        $get = $app->dbSelect($sql);
        die(json_encode($get));
    }

    public function listbookingAction($num, $page, $keyword, $day, $month,$year, $status, $currdate){
        $app = new CB();

        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT schedules.id as schedid, schedules.startdatetime,  schedules.enddatetime,  schedules.scheduledate, schedules.status, members.firstname, members.lastname, members.email, schedules.hourday, schedules.description, reservations.invoiceno,  pages.title, schedules.created_at, pagecategory.colorlegend  FROM reservations INNER JOIN schedules ON invoiceid = invoiceno INNER JOIN members ON reservations.memberid = members.memberid INNER JOIN pages ON pages.pageid=schedules.serviceid INNER JOIN pagecategory ON pages.category=pagecategory.id ';
        $sqlCount = 'SELECT COUNT(*) FROM reservations INNER JOIN schedules ON invoiceid = invoiceno INNER JOIN members ON reservations.memberid = members.memberid INNER JOIN pages ON pages.pageid=schedules.serviceid ';
        $sqlWhereKey = '';
        $sqlWhereDate = '';
        $sqlWhereStatus = '';
        $where ='';

        //Keyword
        if ($keyword != 'null' && $keyword != 'undefined') {
            $sqlWhereKey = " (firstname LIKE '%" . $keyword . "%' OR lastname LIKE '%" . $keyword . "' OR email LIKE '%".$keyword."%' OR description LIKE '%".$keyword."%' OR invoiceno LIKE '%".$keyword."%' OR title = '".$searchdate."') ";
        }

        //Date
        if(!empty($day) && !empty($month) && !empty($year)){
            $sqlWhereDate = " (YEAR(scheduledate) = '".$year."' AND MONTH(scheduledate) = '".$month."' AND DAY(scheduledate)='".$day."') ";
        }elseif(empty($day) && !empty($month) && !empty($year)){
            $sqlWhereDate = " (YEAR(scheduledate) = '".$year."' AND MONTH(scheduledate) = '".$month."') ";
        }elseif(empty($day) && empty($month) && !empty($year)){
            $sqlWhereDate = " (YEAR(scheduledate) = '".$year."') ";
        }

        $where = !empty($sqlWhereKey) || !empty($sqlWhereDate) ? " WHERE " . (!empty($sqlWhereKey) ? $sqlWhereKey . (!empty($sqlWhereDate) ? ' AND '.$sqlWhereDate : ' ') : $sqlWhereDate). ' ' : ' ' ;

        //Status
        if($status == 'UNATTENDED'){
            $where .= (!empty($where) ? " AND ('$currdate' > enddatetime AND '$currdate' > startdatetime) AND schedules.status='RESERVED' " : " WHERE '$currdate' > enddatetime AND '$currdate' > startdatetime AND schedules.status='RESERVED' ");
        }elseif($status == 'ALL'){
            $where .= ' ';
        }elseif($status == 'RESERVED'){
            $where .= (!empty($where) ? " AND ('$currdate' < enddatetime AND '$currdate' < startdatetime) AND schedules.status='RESERVED' " : " WHERE '$currdate' < enddatetime AND '$currdate' < startdatetime AND schedules.status='RESERVED' ");
        }else{
            $where .= (!empty($where) ? " AND schedules.status='$status'" : " WHERE schedules.status='$status' ");
        }

        $sql .= $where;
        $sqlCount .= $where;

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }
        $sql .= " ORDER BY schedules.scheduledate DESC ";
        $sql .= " LIMIT " . $offsetfinal . ",10";
        // getting the query

        //var_dump($sql);

        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);

        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));
    }

}


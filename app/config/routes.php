<?php

/**
 * @author Jete O'Keeffe
 * @version 1.0
 * @link http://docs.phalconphp.com/en/latest/reference/micro.html#defining-routes
 * @eg.
 */

$routes[] = [
	'method' => 'post',
	'route' => '/ping',
	'handler' => ['Controllers\ExampleController', 'pingAction']
];


$routes[] = [
	'method' => 'post',
	'route' => '/test/{id}',
	'handler' => ['Controllers\ExampleController', 'testAction']
];

$routes[] = [
	'method' => 'post',
	'route' => '/skip/{name}',
	'handler' => ['Controllers\ExampleController', 'skipAction'],
    'authentication' => FALSE
];


$routes[] = [
    'method' => 'post',
    'route' => '/map/marker/upload',
    'handler' => ['Controllers\MapController', 'uploadPicsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/map/create/info',
    'handler' => ['Controllers\MapController', 'saveMapMarkerAction'],
    'authentication' => FALSE
];

//booking router
$routes[] = [
    'method' => 'post',
    'route' => '/booking/send',
    'handler' => ['Controllers\BookingController', 'sendbookingAction'],
    'authentication' => FALSE
];


//rainier routers

//pages routers
$routes[] = [
    'method' => 'post',
    'route' => '/pages/saveimage',
    'handler' => ['Controllers\PagesController', 'saveimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pages/listimages',
    'handler' => ['Controllers\PagesController', 'listimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pages/get',
    'handler' => ['Controllers\PagesController', 'getAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/pages/create',
    'handler' => ['Controllers\PagesController', 'createPagesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/pages/createpagecategory',
    'handler' => ['Controllers\PagesController', 'createpagecategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pages/listpagecategory',
    'handler' => ['Controllers\PagesController', 'listpagecategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pages/managepage/{num}/{off}/{keyword}',
    'handler' => ['Controllers\PagesController', 'managePagesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pages/managepagecategory/{num}/{off}/{keyword}',
    'handler' => ['Controllers\PagesController', 'managePagesCategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/pages/updatepagestatus/{status}/{pageid}/{keyword}',
    'handler' => ['Controllers\PagesController', 'pageUpdatestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pages/getpages',
    'handler' => ['Controllers\PagesController', 'getpagesAction'],
    'authentication' => FALSE
];


$routes[] = [
    'method' => 'get',
    'route' => '/page/pagedelete/{pageid}',
    'handler' => ['Controllers\PagesController', 'pagedeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pages/categorydelete/{catid}',
    'handler' => ['Controllers\PagesController', 'categorydeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/pages/updatecategorynames',
    'handler' => ['Controllers\PagesController', 'updatecategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/page/pageedit/{pageid}',
    'handler' => ['Controllers\PagesController', 'pageeditoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/page/pagerightitem/{pageid}',
    'handler' => ['Controllers\PagesController', 'pagerightitemAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/pages/saveeditedpage',
    'handler' => ['Controllers\PagesController', 'saveeditedPagesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/page/getpage/{pageslugs}',
    'handler' => ['Controllers\PagesController', 'getPageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/page/getpagecategory/{pageslugs}',
    'handler' => ['Controllers\PagesController', 'getPagecategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
 'method' => 'post',
 'route' => '/pages/deletepagesimg',
 'handler' => ['Controllers\PagesController', 'deletepagesimgAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/pages/loadcatpages/{catid}',
 'handler' => ['Controllers\PagesController', 'loadcatpagesAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/pages/updatepagescategory',
    'handler' => ['Controllers\PagesController', 'updatepagescatAction'],
    'authentication' => FALSE
];


//TESTEMONIALS FRONTEND
$routes[] = [
    'method' => 'get',
    'route' => '/page/gettestimonial/{pageslugs}',
    'handler' => ['Controllers\TestimonialsController', 'gettestimonialAction'],
    'authentication' => FALSE
];


//MENU FRONTEND
$routes[] = [
    'method' => 'get',
    'route' => '/menu/{cat}',
    'handler' => ['Controllers\PagesController', 'listmenuAction'],
    'authentication' => FALSE
];



//news routers
$routes[] = [
    'method' => 'post',
    'route' => '/news/saveimage',
    'handler' => ['Controllers\NewsController', 'saveimageAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/savevideo',
    'handler' => ['Controllers\NewsController', 'featuredvideoAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/saveauthorimage',
    'handler' => ['Controllers\NewsController', 'saveauthorimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/listimages',
    'handler' => ['Controllers\NewsController', 'listimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/listvideo',
    'handler' => ['Controllers\NewsController', 'listvideoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/authorlistimages',
    'handler' => ['Controllers\NewsController', 'authorlistimagesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/listcategory',
    'handler' => ['Controllers\NewsController', 'listcategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/listtags',
    'handler' => ['Controllers\NewsController', 'listtagsAction'],
    'authentication' => FALSE
];

$routes[] = [
		'method' => 'get',
		'route' => '/tags/loadconflict/{id}',
		'handler' => ['Controllers\NewsController', 'tagconflictAction'],
		'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/news/listauthor',
    'handler' => ['Controllers\NewsController', 'listAuthorAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/displayvideo',
    'handler' => ['Controllers\NewsController', 'displayvideoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/create',
    'handler' => ['Controllers\NewsController', 'createNewsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/createauthor',
    'handler' => ['Controllers\NewsController', 'createAuthorAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/newscenter/create',
    'handler' => ['Controllers\NewsController', 'createNewsCenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/managenews/{num}/{off}/{keyword}/{sort}',
    'handler' => ['Controllers\NewsController', 'manageNewsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/manageauthor/{num}/{off}/{keyword}',
    'handler' => ['Controllers\NewsController', 'manageAuthorAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatenewsstatus/{status}/{newsid}/{keyword}',
    'handler' => ['Controllers\NewsController', 'newsUpdatestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatenewscenterstatus/{status}/{newsid}/{keyword}',
    'handler' => ['Controllers\NewsController', 'newsCenterUpdatestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/newsdelete/{newsid}',
    'handler' => ['Controllers\NewsController', 'newsdeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/authordelete/{authorid}',
    'handler' => ['Controllers\NewsController', 'authordeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/newscenterdelete/{newsid}',
    'handler' => ['Controllers\NewsController', 'newscenterdeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/newsedit/{newsid}',
    'handler' => ['Controllers\NewsController', 'newseditoAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/news/editauthor/{authorid}',
    'handler' => ['Controllers\NewsController', 'authoreditoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/newscenteredit/{newsid}',
    'handler' => ['Controllers\NewsController', 'newscentereditoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatenews',
    'handler' => ['Controllers\NewsController', 'updateNewsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/edieauthor',
    'handler' => ['Controllers\NewsController', 'authorupdateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/newscenter/edit',
    'handler' => ['Controllers\NewsController', 'editNewsCenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/managecategory/{num}/{off}/{keyword}',
    'handler' => ['Controllers\NewsController', 'managecategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/managetags/{num}/{off}/{keyword}',
    'handler' => ['Controllers\NewsController', 'managetagsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/savecategory',
    'handler' => ['Controllers\NewsController', 'createcategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/savetags',
    'handler' => ['Controllers\NewsController', 'createtagsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/addvid',
    'handler' => ['Controllers\NewsController', 'savevideoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatecategorynames/{catname}/{id}',
    'handler' => ['Controllers\NewsController', 'updatecategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatetags/{tagname}/{id}',
    'handler' => ['Controllers\NewsController', 'updatetagsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/categorydelete/{id}',
    'handler' => ['Controllers\NewsController', 'categorydeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/tagsdelete/{id}',
    'handler' => ['Controllers\NewsController', 'tagsdeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/listcategory',
 'handler' => ['Controllers\NewsController', 'listcategoryfrontendAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/featurednews',
 'handler' => ['Controllers\NewsController', 'featurednewsfrontendAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/latest',
 'handler' => ['Controllers\NewsController', 'latestnewsfrontendAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/founderswisdom',
 'handler' => ['Controllers\NewsController', 'founderswisdomfrontendAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/view/{newsslugs}',
 'handler' => ['Controllers\NewsController', 'viewfronentnewsAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/listtags/{newsslugs}',
 'handler' => ['Controllers\NewsController', 'viewfronenttagsAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/listnewsbycategory/{category}/{offset}/{page}',
 'handler' => ['Controllers\NewsController', 'listnewsbycategoryAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/frontend/listnewsbytag/{tag}/{offset}/{page}',
    'handler' => ['Controllers\NewsController', 'listnewsbytagAction'],
    'authentication' => FALSE
];


$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/listnews/{offset}/{page}',
 'handler' => ['Controllers\NewsController', 'listAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/frontend/author/{id}/{offset}/{page}',
    'handler' => ['Controllers\NewsController', 'authorAction'],
    'authentication' => FALSE
];

$routes[] = [
 'method' => 'post',
 'route' => '/news/deletenewsimg',
 'handler' => ['Controllers\NewsController', 'deletenewsimgAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'post',
 'route' => '/news/deletevideo',
 'handler' => ['Controllers\NewsController', 'deletevideoAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'post',
 'route' => '/news/deleteauthorimg',
 'handler' => ['Controllers\NewsController', 'deleteauthorimgAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/loadconflict/{id}',
 'handler' => ['Controllers\NewsController', 'loadconflictsAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'post',
 'route' => '/news/updateconflict',
 'handler' => ['Controllers\NewsController', 'updateconflictAction'],
 'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/tags/updateconflict',
	'handler' => ['Controllers\NewsController', 'updateTagConflictAction'],
	'authentication' => FALSE
];

$routes[] = [
 'method' => 'post',
 'route' => '/news/updateconflict',
 'handler' => ['Controllers\NewsController', 'updateconflictAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/frontend/archives',
    'handler' => ['Controllers\NewsController', 'listarchiveAction'],
    'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/loadauthornews/{id}',
 'handler' => ['Controllers\NewsController', 'loadauthornewsAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'post',
 'route' => '/news/updatenewsauthors',
 'handler' => ['Controllers\NewsController', 'updatenewsauthorsAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/frontend/listnewsbyarchive/{month}/{year}/{offset}/{page}',
    'handler' => ['Controllers\NewsController', 'listnewsbyarchiveAction'],
    'authentication' => FALSE
];
/** Kyben
 */
/* Validation for username exist */
$routes[] = [
    'method' => 'get',
    'route' => '/validate/username/{name}/{id}',
    'handler' => ['Controllers\UserController', 'userExistAction'],
    'authentication' => FALSE
];
/* Validation for email exist */
$routes[] = [
    'method' => 'get',
    'route' => '/validate/email/{name}/{id}',
    'handler' => ['Controllers\UserController', 'emailExistAction'],
    'authentication' => FALSE
];
/* Validation for old password */$routes[] = [
		'method' => 'get',
		'route' => '/validate/password/{id}/{password}',
		'handler' => ['Controllers\UserController', 'validatePasswordAction'],
		'authentication' => FALSE
];
/* Submit User Regester */
$routes[] = [
    'method' => 'post',
    'route' => '/user/register',
    'handler' => ['Controllers\UserController', 'registerUserAction'],
    'authentication' => FALSE
];
/* List all User */
$routes[] = [
    'method' => 'get',
    'route' => '/user/list/{num}/{off}/{keyword}/{id}',
    'handler' => ['Controllers\UserController', 'userListAction'],
    'authentication' => FALSE
];
/* User Info */
$routes[] = [
    'method' => 'get',
    'route' => '/user/info/{id}',
    'handler' => ['Controllers\UserController', 'userInfoction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/user/update',
    'handler' => ['Controllers\UserController', 'userUpdateAction'],
    'authentication' => FALSE
];
/* DELETE User */
$routes[] = [
    'method' => 'get',
    'route' => '/user/delete/{id}',
    'handler' => ['Controllers\UserController', 'deleteUserAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/user/activation',
    'handler' => ['Controllers\UserController', 'activationAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/user/login/{username}/{password}',
    'handler' => ['Controllers\UserController', 'loginAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/user/putaka',
    'handler' => ['Controllers\UserController', 'putaAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/forgotpassword/reset',
    'handler' => ['Controllers\UserController', 'resetpasswordAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/checktoken/check/{email}/{token}',
    'handler' => ['Controllers\UserController', 'changepasswordAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/forgotpassword/updatepassword',
    'handler' => ['Controllers\UserController', 'updatepasswordAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/user/updatestatus/{id}/{newstat}',
    'handler' => ['Controllers\UserController', 'updatestatusAction'],
    'authentication' => FALSE
];
$routes[] = [
		'method' => 'get',
		'route' => '/user/delete/multiple/{id}',
		'handler' => ['Controllers\UserController', 'deleteUsersAction'],
		'authentication' => FALSE
];
// Testimonials
$routes[] = [
    'method' => 'get',
    'route' => '/testimonials/list/{num}/{off}/{keyword}',
    'handler' => ['Controllers\TestimonialsController', 'listAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/testimonials/create',
    'handler' => ['Controllers\TestimonialsController', 'createAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/testimonials/get/{id}',
    'handler' => ['Controllers\TestimonialsController', 'getAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/testimonials/update',
    'handler' => ['Controllers\TestimonialsController', 'updateAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/testimonials/delete/{id}',
    'handler' => ['Controllers\TestimonialsController', 'deleteAction'],
    'authentication' => FALSE
];
// End Testimonials

$routes[] = [
    'method' => 'get',
    'route' => '/booking/list/{num}/{off}/{keyword}/{searchdate}',
    'handler' => ['Controllers\BookingController', 'listAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/get/{id}',
    'handler' => ['Controllers\BookingController', 'getAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/read/{id}',
    'handler' => ['Controllers\BookingController', 'readAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/booking/reply',
    'handler' => ['Controllers\BookingController', 'replyAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/getreplies/{id}',
    'handler' => ['Controllers\BookingController', 'getrepliesAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/delete/{id}',
    'handler' => ['Controllers\BookingController', 'deleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/booking/offline/reservation/add',
    'handler' => ['Controllers\BookingController', 'addreservationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/schedule/validate/{stime}/{etime}',
    'handler' => ['Controllers\BookingController', 'validateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/member/getbyemail/{email}',
    'handler' => ['Controllers\BookingController', 'getbyemailAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/invoice/get/{invoiceno}',
    'handler' => ['Controllers\BookingController', 'getinvoiceAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/getmonthevents/{month}/{year}',
    'handler' => ['Controllers\BookingController', 'getmontheventsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/booking/updateshedule',
    'handler' => ['Controllers\BookingController', 'updatesheduleAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/voidschedule/{id}/{status}',
    'handler' => ['Controllers\BookingController', 'updatestatusAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/countingstatus/{currtime}',
    'handler' => ['Controllers\BookingController', 'countingstatusAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/listschedule/{num}/{off}/{keyword}/{day}/{month}/{year}/{status}/{currdate}',
    'handler' => ['Controllers\BookingController', 'listbookingAction'],
    'authentication' => FALSE
];

// Prices
$routes[] = [
    'method' => 'post',
    'route' => '/service/price/add',
    'handler' => ['Controllers\PagesController', 'addpriceAction'],
    'authentication' => FALSE
];

// Settings
    /*google analytics*/
$routes[] = [
    'method' => 'get',
    'route' => '/settings/load',
    'handler' => ['Controllers\SettingsController', 'loadsettingsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/settings/update/googlescript',
    'handler' => ['Controllers\SettingsController', 'updatescriptAction'],
    'authentication' => FALSE
];

    /*maintenance*/
$routes[] = [
    'method' => 'post',
    'route' => '/settings/save/maintenance',
    'handler' => ['Controllers\SettingsController', 'savemaintenanceAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/settings/off/maintenance',
    'handler' => ['Controllers\SettingsController', 'offmaintenanceAction'],
    'authentication' => FALSE
];

/*Contact us module*/
$routes[] = [
    'method' => 'post',
    'route' => '/contactus/save',
    'handler' => ['Controllers\ContactusController', 'sendAction'],
    'authentication' => FALSE
];
$routes[] = [
		'method' => 'get',
		'route' => '/contactus/list/{num}/{off}/{keyword}/{searchdate}',
		'handler' => ['Controllers\ContactusController', 'listAction'],
		'authentication' => FALSE
];
$routes[] = [
		'method' => 'get',
		'route' => '/contactus/delete/{id}',
		'handler' => ['Controllers\ContactusController', 'deleteAction'],
		'authentication' => FALSE
];
$routes[] = [
		'method' => 'get',
		'route' => '/contactus/get/{id}',
		'handler' => ['Controllers\ContactusController', 'getAction'],
		'authentication' => FALSE
];
$routes[] = [
		'method' => 'get',
		'route' => '/contactus/read/{id}',
		'handler' => ['Controllers\ContactusController', 'readAction'],
		'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/contactus/getreplies/{id}',
    'handler' => ['Controllers\ContactusController', 'getrepliesAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/contactus/reply',
    'handler' => ['Controllers\ContactusController', 'replyAction'],
    'authentication' => FALSE
];

//metadata
$routes[] = [
    'method' => 'get',
    'route' => '/meta/managepage/{num}/{off}/{keyword}',
    'handler' => ['Controllers\MetadataController', 'listAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/meta/edit',
    'handler' => ['Controllers\MetadataController', 'editAction'],
    'authentication' => FALSE
];
//meta front
$routes[] = [
    'method' => 'get',
    'route' => '/meta/show/{data}',
    'handler' => ['Controllers\MetadataController', 'showAction'],
    'authentication' => FALSE
];
//meta front
$routes[] = [
    'method' => 'get',
    'route' => '/meta/showcat/{data}',
    'handler' => ['Controllers\MetadataController', 'showcatAction'],
    'authentication' => FALSE
];

return $routes;
